<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('user_type');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('status');

            $table->timestamp('last_sign_in_at')->nullable();
            $table->timestamp('current_sign_in_at')->nullable();
            $table->string('user_click');
            $table->timestamp('user_click_time')->nullable();

            //admin-sub_admin
            $table->boolean('categories_access');
            $table->boolean('products_access');
            $table->boolean('orders_access');
            $table->boolean('users_access');
            //admin-sub_admin



            $table->rememberToken();
            $table->timestamps();

             });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
