<?php

use Illuminate\Database\Seeder;

class PincodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pincodes')->delete();
        $pincodesRecords =[

            ['id'=>1,'pincode'=>'1212','city'=>'Dhaka', 'state'=>'Khilkhet'],
            ['id'=>2,'pincode'=>'1000','city'=>'Dhaka', 'state'=>'Malibag'],
            ['id'=>3,'pincode'=>'2000','city'=>'Dhaka', 'state'=>'Airport'],
            ['id'=>4,'pincode'=>'3000','city'=>'Dhaka', 'state'=>'kaula'],
            ['id'=>5,'pincode'=>'4000','city'=>'Dhaka', 'state'=>'Barisal'],
            ['id'=>6,'pincode'=>'5000','city'=>'Dhaka', 'state'=>'Rajsashi'],
            ['id'=>7,'pincode'=>'6000','city'=>'Dhaka', 'state'=>'Fardabad'],
            ['id'=>8,'pincode'=>'7000','city'=>'Dhaka', 'state'=>'Fardavad'],
            ['id'=>9,'pincode'=>'8000','city'=>'Dhaka', 'state'=>'Dha'],
            ['id'=>10,'pincode'=>'1229','city'=>'Dhaka', 'state'=>'Farmgate'],

            ];
        DB::table('pincodes')->insert($pincodesRecords);
    }
}
