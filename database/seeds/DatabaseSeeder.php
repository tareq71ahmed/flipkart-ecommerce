<?php

use Illuminate\Database\Seeder;
use App\Category;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminTableSeeder::class);
        $this->call(CmsPageSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(PincodesSeeder::class);
        $this->call(CodPincodesSeeder::class);
        $this->call(PrepaidPincodesSeeder::class);
        $this->call(ShippingChargesSeeder::class);
        $this->call(ProfileSeeder::class);
        factory('App\User',5)->create();
        factory('App\Category',20)->create();
        factory('App\Cms',5)->create();
        factory('App\Pincode',20)->create();
        factory('App\Prepaid_pincode',20)->create();
        factory('App\cod_pincode',20)->create();
        factory('App\Coupon',5)->create();
        factory('App\Currencies',20)->create();
        factory('App\Delivery',20)->create();
        factory('App\Order',7)->create();
        factory('App\Product',15)->create();
        factory('App\Product_attribute',7)->create();
        factory('App\Order_product',7)->create();
        factory('App\Product_image',7)->create();
        factory('App\ShippingCharge',100)->create();
        factory('App\Banner',2)->create();
        factory('App\Cart',7)->create();


    }
}
