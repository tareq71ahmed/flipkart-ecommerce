<?php

use Illuminate\Database\Seeder;

class ShippingChargesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shipping_charges')->delete();
        $shipping_charges_Records =[
            ['id'=>1,'country'=>'Bangladesh','shipping_charges0_500g'=>'500', 'shipping_charges501_1000g'=>'1000',
                'shipping_charges1001_2000g'=>'2000','shipping_charges2001_5000g'=>'3000'],

            ['id'=>2,'country'=>'India','shipping_charges0_500g'=>'500', 'shipping_charges501_1000g'=>'1000',
                'shipping_charges1001_2000g'=>'2000','shipping_charges2001_5000g'=>'3000'],

            ['id'=>3,'country'=>'China','shipping_charges0_500g'=>'500', 'shipping_charges501_1000g'=>'1000',
                'shipping_charges1001_2000g'=>'2000','shipping_charges2001_5000g'=>'3000'],


            ['id'=>4,'country'=>'USA','shipping_charges0_500g'=>'500', 'shipping_charges501_1000g'=>'1000',
            'shipping_charges1001_2000g'=>'2000','shipping_charges2001_5000g'=>'3000'],

        ];
        DB::table('shipping_charges')->insert($shipping_charges_Records);

    }
}
