<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //password :  $2y$10$u7fOatQjHxg/LGHiZ54ZQeOGilvKUD.zE39muuLuMuXECLB4Hqx7y
        //password :  12345678

        DB::table('admins')->delete();
        $adminRecords =[
          ['id'=>1,'name'=>'admin','user_type'=>'admin', 'email'=>'admin@gmail.com','password'=>'$2y$10$u7fOatQjHxg/LGHiZ54ZQeOGilvKUD.zE39muuLuMuXECLB4Hqx7y','status'=>'1'],
          ['id'=>2,'name'=>'super','user_type'=>'admin', 'email'=>'super@gmail.com','password'=>'$2y$10$u7fOatQjHxg/LGHiZ54ZQeOGilvKUD.zE39muuLuMuXECLB4Hqx7y','status'=>'1']
        ];

        DB::table('admins')->insert($adminRecords);
    }
}
