<?php

use Illuminate\Database\Seeder;

class CodPincodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // cod_pincodes


        DB::table('cod_pincodes')->delete();
        $codpincodesRecords =[

            ['id'=>1,'pincode'=>'1212','city'=>'Dhaka', 'state'=>'Khilkhet'],
            ['id'=>2,'pincode'=>'1000','city'=>'Dhaka', 'state'=>'Malibag'],
            ['id'=>3,'pincode'=>'2000','city'=>'Dhaka', 'state'=>'Airport'],
            ['id'=>4,'pincode'=>'3000','city'=>'Dhaka', 'state'=>'kaula'],
            ['id'=>5,'pincode'=>'4000','city'=>'Dhaka', 'state'=>'kaula'],
            ['id'=>6,'pincode'=>'5000','city'=>'Dhaka', 'state'=>'kaula'],
            ['id'=>7,'pincode'=>'1229','city'=>'Dhaka', 'state'=>'Farmgate'],

        ];
        DB::table('cod_pincodes')->insert($codpincodesRecords);


    }
}
