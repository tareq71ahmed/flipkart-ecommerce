<?php

use Illuminate\Database\Seeder;

class CmsPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cms')->delete();
        //apply on front_footer.blade.php

        $CmsPageRecords =[


            ['id'=>1,'title'=>'about-us','description'=>'About-us page Coming Soon',
                'url'=>'about-us', 'meta_title'=>'about-us',
                'meta_description'=>'Coming Soon',
                'meta_keywords'=>'about-us',
                'status'=>'1'
            ],


            ['id'=>2,'title'=>'Career','description'=>'Career page on the way',
                'url'=>'career', 'meta_title'=>'career',
                'meta_description'=>'career',
                'meta_keywords'=>'career',
                'status'=>'1'
            ],

            ['id'=>3,'title'=>'location','description'=>'location page on the way',
                'url'=>'location', 'meta_title'=>'location',
                'meta_description'=>'location',
                'meta_keywords'=>'location',
                'status'=>'1'
            ],

            ['id'=>4,'title'=>'copyright','description'=>'copyright page on the way',
                'url'=>'copyright', 'meta_title'=>'copyright',
                'meta_description'=>'copyright page on the way',
                'meta_keywords'=>'copyright',
                'status'=>'1'
            ],



        ];

        DB::table('cms')->insert($CmsPageRecords);
    }
}
