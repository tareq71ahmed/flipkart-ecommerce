<?php

use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('profiles')->delete();
        $profileRecords =[
            ['id'=>1,'user_id'=>'1','address'=>'Marlin Lake', 'city'=>'sydney','state'=>'Oregon','country'=>'Kiribati','pincode'=>'1000','mobile'=>'815-835-5556'],
            ['id'=>2,'user_id'=>'2','address'=>'Dhaka', 'city'=>'Dhaka','state'=>'Namapara','country'=>'Bangladesh','pincode'=>'2000','mobile'=>'715-805-59856'],
            ['id'=>3,'user_id'=>'3','address'=>'Khilkhet', 'city'=>'Dhaka','state'=>'Middle khilkhet','country'=>'Bangladesh','pincode'=>'3000','mobile'=>'7815-846-5536'],
            ['id'=>4,'user_id'=>'4','address'=>'Farmgate', 'city'=>'Dhaka','state'=>'Green road','country'=>'Bangladesh','pincode'=>'4000','mobile'=>'402-835-5766'],
            ['id'=>5,'user_id'=>'5','address'=>'Airport', 'city'=>'Dhaka','state'=>'Airport','country'=>'Bangladesh','pincode'=>'5000','mobile'=>'325-765-5856'],

        ];

        DB::table('profiles')->insert($profileRecords);


    }
}
