<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
//guard
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
//guard End

class Admin extends Authenticatable
{

    use Notifiable;
    protected $guard ='admin';
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $guarded=[];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


}
