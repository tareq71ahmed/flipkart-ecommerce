<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Route;
use Closure;
use Session;
use App\Admin;
use Auth;

class CheckRole
{

    public function handle($request, Closure $next)
    {
        if (Auth::guard('admin')->check() && Admin::where(['status' => '1'])  ) {

            $adminDetails = Admin::where(['email' =>Auth::guard('admin')->user()->email] )->first();
             $userType = $adminDetails->user_type;


            if ($userType === "admin") {

                $adminDetails->categories_access = 1;
                $adminDetails->products_access = 1;
                $adminDetails->orders_access = 1;
                $adminDetails->users_access = 1;
            }



            // Get Current Path
            $currentPath = Route::getFacadeRoot()->current()->uri();


            if ($currentPath === "admin/add-category" && $adminDetails->categories_access == 0) {
                return redirect('/admin/dashboard')->with('error', 'You have no access for this module');
            }


            if ($currentPath === "admin/view-category" && $adminDetails->categories_access == 0) {
                return redirect('/admin/dashboard')->with('error', 'You have no access for this module');
            }


            if ($currentPath === "admin/add-product" && $adminDetails->products_access == 0) {
                return redirect('/admin/dashboard')->with('error', 'You have no access for this module');

            }


            if ($currentPath === "admin/view-product" && $adminDetails->products_access == 0) {
                return redirect('/admin/dashboard')->with('error', 'You have no access for this module');
            }


            if ($currentPath === "admin/user/view-orders" && $adminDetails->orders_access == 0) {
                return redirect('/admin/dashboard')->with('error', 'You have no access for this module');
            }


            if ($currentPath === "admin/user/all-user" && $adminDetails->users_access== 0) {
                return redirect('/admin/dashboard')->with('error', 'You have no access for this module');

            }


            return $next($request);

        }
        return redirect('admin/login');

    }




}
