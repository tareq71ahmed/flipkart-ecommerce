<?php

namespace App\Http\Middleware;
use App\User;
use Illuminate\Support\Facades\Auth;
use Closure;
use Carbon\Carbon;

class Frontlogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next )
    {
//        Auth('web') AND Auth::check() AND Auth::guard('web')->check()

        if (  Auth::check() && Auth::user()->user_type=='seeker' && Auth::user()->status=='1' ){
           $request->path(); // path
           $request->route()->getName();
           $mytime = Carbon::now();
           User::where('id',Auth::user()->id)->update(['user_click'=> $request->path(),'user_click_time'=>$mytime]);


            return $next($request);
        }
        return redirect('/login');

    }







}
