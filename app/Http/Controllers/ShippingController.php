<?php

namespace App\Http\Controllers;

use App\ShippingCharge;
use Illuminate\Http\Request;

class ShippingController extends Controller
{

    public function shipping()
    {
        $shipping_charges =ShippingCharge::get();
        return view('admin.shipping.view_shipping',compact('shipping_charges'));
    }


    public function edit_shipping($id)
    {
        $shipping = ShippingCharge::find($id);
        return view('admin.shipping.edit_shipping',compact('shipping'));
    }


    public function update_shipping(Request $request ,$id)
    {
        $shipping = ShippingCharge::where('id',$id)->first();
        $shipping->shipping_charges0_500g=$request['shipping_charges0_500g'];
        $shipping->shipping_charges501_1000g=$request['shipping_charges501_1000g'];
        $shipping->shipping_charges1001_2000g=$request['shipping_charges1001_2000g'];
        $shipping->shipping_charges2001_5000g=$request['shipping_charges2001_5000g'];
        $shipping->save();
        return back()->with('success','Shipping Information updated Successfully!');

    }





}
