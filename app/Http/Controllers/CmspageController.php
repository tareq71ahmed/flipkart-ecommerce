<?php

namespace App\Http\Controllers;

use App\Cms;
use Illuminate\Http\Request;
use DB;

class CmspageController extends Controller
{

    //apply on front_footer.blade.php

    public function cms()
    {
        return view('cms.cms_page');
    }

    public function add_cms(Request $request)
    {

        //validation
        $this->validate($request,[
            'title'            => 'required|string',
            'url'              => 'required|string|unique:cms',
            'description'      => 'required|string',

        ]);
        //validation End



        $cms                      =    New Cms();

        //dupicate url prevent
        $counturl = DB::table('cms')->where(['url' => $request['url']])->count();
        if ($counturl > 0) {
            return redirect()->back()->with('error', 'Url already exist !');
        }
        //dupicate url prevent end

        $cms -> title             =    $request['title'];
        $cms -> url               =    $request['url'];
        $cms -> description       =    $request['description'];
        $cms -> meta_title        =    $request['meta_title']??"";
        $cms -> meta_description  =    $request['meta_description']??"";
        $cms -> meta_keywords     =    $request['meta_keywords']??"";
        $cms -> status            =    $request['status'] ?? " ";
        $cms -> save();
        return back() ->with('success','CMS Added Successfully');
    }




    public   function  view_cms()
    {
        $cms_pages = Cms::all();
        return view('cms.view_cms_page',compact('cms_pages'));
    }


    public  function edit_cms($id)
    {
        $cms =Cms::find($id);
        $cms_status = Cms::where(['id'=>$id])->first();
        return view('cms.edit_cms',compact('cms','cms_status'));
    }



    public function update_cms(Request  $request, $id)
    {

        //validation
        $this->validate($request,[
            'title'            => 'required|string',
            'url'              => 'required|string',
            'description'      => 'required|string',


        ]);
        //validation End

        $cms                      =    Cms::where('id',$id)->first();
        $cms -> title             =    $request['title'];
        $cms -> url               =    $request['url'];
        $cms -> description       =    $request['description'];
        $cms -> meta_title        =    $request['meta_title']?? "";
        $cms -> meta_description  =    $request['meta_description']??"";
        $cms -> meta_keywords     =    $request['meta_keywords']??"";
        $cms -> status            =    $request['status'] ?? " ";
        $cms -> save();
        return back() ->with('success','CMS updated Successfully');
    }




    public  function  delete_cms($id)
    {
        $cms   = Cms::where('id',$id)->first();
        $cms   -> delete();
        return back()->with('success','CMS Delete Successfully!');
    }






}
