<?php

namespace App\Http\Controllers;

use App\Delivery;
use App\Order;
use App\Order_product;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Session;
class UserOrderProductController extends Controller
{

    public function  viewOrders()
    {


//        if(Session::get('adminDetails')['orders_access'] == 0 )
//        {
//            return redirect('/admin/dashboard')->with('error', 'You have no access for this module');
//        }


        $orders = Order::with('order_products')->get();
        return view('admin.orders.view_orders',compact('orders'));
    }


    public  function viewOrderDetails($order_id)
    {

//        if(Session::get('adminDetails')['orders_access'] == 0 )
//        {
//            return redirect('/admin/dashboard')->with('error', 'You have no access for this module');
//        }

        $ordersDetails = Order::with('order_products')->where('id',$order_id)->first();
        $user_id = $ordersDetails->user_id;
        $billing_address = Profile::with('user')->where('user_id',$user_id)->first();
        $shipping_address = Delivery::where('user_id',$user_id)->first();
        return view('admin.orders.order_details',compact('ordersDetails','billing_address','shipping_address'));

    }


    public  function  updateOrderStatus(Request $request)
    {
//        if(Session::get('adminDetails')['orders_access'] == 0 )
//        {
//            return redirect('/admin/dashboard')->with('error', 'You have no access for this module');
//        }
            Order::where('id',$request['order_id'])->update(['order_status'=>$request['order_status']]);
            return redirect()->back()->with('flash_message_success','Order Status has been updated successfully!');

    }




    public function viewOrderinvoice($order_id)
    {
//        if(Session::get('adminDetails')['orders_access'] == 0 )
//        {
//            return redirect('/admin/dashboard')->with('error', 'You have no access for this module');
//        }

        $ordersDetails = Order::with('order_products')->where('id',$order_id)->first();
        $user_id = $ordersDetails->user_id;
        $billing_address = Profile::with('user')->where('user_id',$user_id)->first();
        $shipping_address = Delivery::where('user_id',$user_id)->first();

        return view('admin.orders.order_invoice',compact('ordersDetails','billing_address','shipping_address'));
    }






}
