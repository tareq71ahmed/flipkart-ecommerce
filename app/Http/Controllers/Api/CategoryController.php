<?php

namespace App\Http\Controllers\Api;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public  function viewcategory(){
       // php artisan make:controller Api/CategoryController

        return Category::all();

    }
}
