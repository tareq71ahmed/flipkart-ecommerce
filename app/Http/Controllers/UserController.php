<?php

namespace App\Http\Controllers;


use App\Delivery;
use App\Mail\VerificationEmail;
use App\Profile;
use Illuminate\Http\Request;
use App\User;
use App\Category ;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use DB;



class UserController extends Controller
{
    public function login()
    {
        $categories = Category::with('categories')->where(['parent_id' => 0, 'status' => 1])->get();
        return view('auth.user-register',compact('categories'));
    }

    public function register()
    {
        $categories = Category::with('categories')->where(['parent_id' => 0, 'status' => 1])->get();
        return view('auth.user-register',compact('categories'));
    }



    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255|min:3',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }


    //    admin registration
    public function userregistration(Request $request){

        $remember_token = Session::get('remember_token');
        if (!isset($remember_token)) {
            $remember_token = str_random(50);
            Session::put('remember_token', $remember_token);
        }

        $user_count = User::Where(['email'=>request('email')])->count();
        if ($user_count > 0) {
            return redirect()->back()->with('error', ' Email Already Exists !');
        } else {

            $user= User::create([
                'name' => request('name'),
                'email' => request('email'),
                'user_type' => request('user_type'),
                'remember_token' => Session::get('remember_token') ?? " ",
//                date_default_timezone_set('Asia/Dhaka'),
//                 $user->created_at = date("Y-m-d H:i:s"),
//                $user->updated_at = date("Y-m-d H:i:s"),
                'password' => Hash::make(request('password')),
            ]);
            Profile::create([
                'user_id'=>$user->id,
            ]);

        }

        // Send Confirmation Email
        $email = $request['email'];
        $messageData = ['email'=>$request['email'],'name'=>$request['name'],'code'=>base64_encode($request['email'])];
        Mail::send('emails.confirmation',$messageData,function($message) use($email){
            $message->to($email)->subject('Confirm your E-com Account');
        });





         return back()->with('success','Please Check Your Mail To Confirm Your Account');
        //return redirect()->back();

    }


    //User login
    public function userLogin(Request $request){
        if ($request->ismethod('post')) {
            $data = $request->input();



                if (  Auth::guard('web')->attempt(['email' => $data['email'], 'password' => $data['password'],'user_type'=>'seeker', 'status' => '1'])) {

                    //session
                    Session::put('frontSession', $data['email']);
                    if(!empty(Session::get('session_id'))){
                        $session_id = Session::get('session_id');
                        DB::table('carts')->where('session_id',$session_id)->update(['user_email' => $data['email']]);
                    }
                    //session End
                    return redirect()->route('cart')->with('success','Successfully LogIn.');



//            if (  Auth::attempt(['email' => $data['email'], 'password' => $data['password'],'user_type'=>'seeker', 'status' => '1'])) {
//                return redirect()->route('cart')->with('success','Successfully LogIn.');
//
            } else {

                return back()->with('error',' Invalid UserName Or Password.Maybe Your Account not Activated');
            }

        }

    }



    //admin check pasword
    public function chkPassword(Request $request){
        $data = $request->all();
        $current_password = $data['current_pwd'];
        $check_password = User::where(['status'=>'1','user_type'=>'seeker','email' => Auth::user()->email])->first();
        if(Hash::check($current_password,$check_password->password)){
            echo "true"; die;
        }else {
            echo "false"; die;
        }
    }



    //update password
    public function updatePassword(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;
            $check_password = User::where(['status'=>'1','user_type'=>'seeker','email' => Auth::user()->email])->first();
            $current_password = $data['current_pwd'];
            if(Hash::check($current_password,$check_password->password)){
                $password = bcrypt($data['new_pwd']);
                User::where(['status'=>'1','user_type'=>'seeker','email' => Auth::user()->email])->update(['password'=>$password]);
                return redirect('user/account')->with('success','Password updated Successfully!');
            }else {
                return redirect('user/account')->with('error','Incorrect Current Password!');
            }

        }
    }


      //Confirm Account
    public function confirmAccount($email){
        $email = base64_decode($email);
        $userCount = User::where('email',$email)->count();
        if($userCount > 0){
            $userDetails = User::where('email',$email)->first();
            if($userDetails->status == 1){
                return back()->with('success','Your Email account is already activated. You can login now.');
            }else{
                User::where('email',$email)->update(['status'=>1,'email_verified_at'=>now()]);

                // Send Welcome Email
                $messageData = ['email'=>$email,'name'=>$userDetails->name];
                Mail::send('emails.welcome',$messageData,function($message) use($email){
                    $message->to($email)->subject('Welcome to E-com Website');
                });

                return back()->with('success','Your Email account is activated. You can login now.');
            }
        }else{
            abort(404);
        }
    }


    //Forgot Password
    public function forgotPassword(Request $request){
        if($request->isMethod('post')) {
            $data = $request->all();
            /*echo "<pre>"; print_r($data); die;*/
            $userCount = User::where('email',$data['email'])->count();
            if($userCount == 0){
                return redirect()->back()->with('flash_message_error','Email does not exists!');
            }

            //Get User Details
            $userDetails = User::where('email',$data['email'])->first();

            //Generate Random Password
            $random_password = str_random(8);

            //Encode/Secure Password
            $new_password = bcrypt($random_password);

            //Update Password
            User::where('email',$data['email'])->update(['password'=>$new_password]);

            //Send Forgot Password Email Code
            $email = $data['email'];
            $name = $userDetails->name;
            $messageData = [
                'email'=>$email,
                'name'=>$name,
                'password'=>$random_password
            ];
            Mail::send('emails.forgotpassword',$messageData,function($message)use($email){
                $message->to($email)->subject('New Password - E-com Website');
            });

            return back()->with('success','Please check your email for new password!');

        }
        $categories = Category::with('categories')->where(['parent_id' => 0, 'status' => 1])->get();
        return view('users.forgot_password',compact('categories'));
    }





    //logout
    public function  logout(){

        Auth::guard('web')->logout();
        //Auth('user')->logout();
       // Auth::guard()->logout();
        return redirect('/login')->with('error','You Logout successfully');
    }








}
