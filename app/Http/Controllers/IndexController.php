<?php

namespace App\Http\Controllers;

use App\Category;
use App\Cms;
use App\Delivery;
use App\Order;
use App\Order_product;
use App\Product;
use App\Product_attribute;
use App\Cart;
use App\Profile;
use App\ShippingCharge;
//use Illuminate\Support\Facades\Session;
use Session;
use DB;
use App\Coupon;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;



class IndexController extends Controller
{

    public function index()
    {
       // $date=date('Y-m-d');
        //$productsAll = Product::where(['feature_item'=> 1,'status'=>1])->get();
        //Descending order
        //  $productsAll = Product::orderBy('id','DESC')->get();

        $productsAll = Product::where(['status' => 1,'feature_item'=> 1])->paginate(12);
        $categories = Category::with('categories')->where(['parent_id' => 0])->get();
        // Meta tags
        $meta_title = "E-shop Sample Website";
        $meta_description = "Online Shopping Site for Men, Women and Kids Clothing";
        $meta_keywords = "eshop website, online shopping, men clothing";


        return view('index', compact('productsAll', 'categories','meta_title','meta_description','meta_keywords'));

    }


    public function listing($url)
    {

        $listing_categories = Category::where(['url' => $url, 'status' => 1])->first();

        if ($listing_categories) {
            $products_listing = Product::where(['category_id' => $listing_categories->id, 'status' => 1])->paginate(6);

        } else {
            $categories = Category::with('categories')->where(['parent_id' => 0, 'status' => 1])->get();
            return view('error', compact('categories'));
        }
        $categories = Category::with('categories')->where(['parent_id' => 0])->get();

        //meta tags
        $meta_title = $listing_categories->meta_title;
        $meta_description = $listing_categories->meta_description;
        $meta_keywords = $listing_categories->meta_keywords;
        //meta tags end

        return view('products.listing', compact('categories', 'products_listing', 'listing_categories','meta_title','meta_description','meta_keywords'));
    }


    public function productsdetails(Product $product)
    {
        // Product Detail Page | Recommended/Related Items/YOU MAY ALSO LIKE/Related Products
        $recommended_products = Product::where(['category_id' => $product->category_id, 'status' => 1])->get();
        // Product Detail Page | Recommended/Related Items/YOU MAY ALSO LIKE/Related Products End


        $categories = Category::with('categories')->where(['parent_id' => 0, 'status' => 1])->get();
        $productAltImages = Product::with('images')->get();
        $prices = Product_attribute::where(['product_id' => $product->id])->get();
        $first_attr_price = Product_attribute::where(['product_id' => $product->id])->first();

        //mera tags
        $productDetails = Product::where(['id' => $product->id])->first();
        $meta_title = $productDetails->product_name;
        $meta_description = $productDetails->description;
        $meta_keywords = $productDetails->product_name;
        //mera tags


        return view('products.detail', compact('categories', 'product', 'productAltImages', 'recommended_products', 'prices', 'first_attr_price','meta_title','meta_description','meta_keywords'));

    }


    public function price_display_on_size(Request $request)
    {

        $product_id = $request->product_id;
        $product_size = $request->product_size;

        //    return  response()->json([
        //         'product_att_price' => $product_size
        //     ]);

        $product_att_price = Product_attribute::where('size', $product_size)
            ->where('product_id', $product_id)->first();

        if (!empty($product_att_price)) {
            return response()->json([
                'product_att_price' => $product_att_price
            ]);
        } else {
            return response()->json([
                'product_att_price' => null
            ]);
        }
    }


    //Add to cart

    public function addtocart(Request $request)
    {
//         dd($request);
        if( !$request["size"] && !$request["product_id"] ) {
            return redirect()->back()->with('error', 'Product already exist in Cart!');
        }

        if( $request["size"] =='' ) {
            return redirect()->back()->with('error', 'please select Your Prefer Size!');
        }

        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        $cart = new Cart();
        $session_id = Session::get('session_id');
        if (!isset($session_id)) {
            $session_id = str_random(40);
            Session::put('session_id', $session_id);
        }
        //check product Quantity is Avlaible or not
        $getProductStock = Product_attribute::where(['product_id' => $request['product_id'], 'size' => $request['size']])->get()->first();
//         dd($getProductStock);
        if ($getProductStock->stock < $request['quantity']) {
            return redirect()->back()->with('error', 'Required Quantity is not available!');
        }
        //check product Quantity is Avlaible or not End


        //  Prevent Duplicate Items


        //        if(empty(Auth::check())){
        //            $countProducts = DB::table('carts')->where(['product_id' => $request['product_id'],'product_color' => $request['product_color'],'size' => $request['size'],'session_id' => $session_id])->count();
        //            if($countProducts>0){
        //                return redirect()->back()->with('flash_message_error','Product already exist in Cart!');
        //            }
        //        }else{
        //            $countProducts = DB::table('carts')->where(['product_id' => $request['product_id'],'product_color' => $request['product_color'],'size' => $request['size'],'user_email' => $request['user_email']])->count();
        //            if($countProducts>0){
        //                return redirect()->back()->with('flash_message_error','Product already exist in Cart!');
        //            }
        //        }

        //      Prevent Duplicate Items End


        // Prevent Duplicate Items
        $countProducts = DB::table('carts')->where(['product_id' => $request['product_id'], 'product_color' => $request['product_color'], 'size' => $request['size'], 'session_id' => $session_id])->count();
        if ($countProducts > 0) {
            return redirect()->back()->with('error', 'Product already exist in Cart!');
        }
        //
        // Prevent Duplicate Items End
        $cart->product_id = $request['product_id'];
        $cart->product_name = $request['product_name'];
        $cart->product_code = $request['product_code'];
        $cart->product_color = $request['product_color'];
        $cart->price = $getProductStock->price;
        $cart->size = $getProductStock->size;
        $cart->sku = $getProductStock->sku;
        //$cart->sku = $request['sku'];
        $cart->quantity = $request['quantity'];
        $cart->session_id = Session::get('session_id') ?? " ";
        if (!empty(Auth::user()->email)) {
            $cart->user_email = Auth::user()->email;
        } else {

            $cart->user_email = '';
        }


//        if (!empty(['session_id'])){
//            $cart->session_id = Session::get('session_id');
//        } else {
//            $cart->session_id= '';
//        }

       // return $cart;
        $cart->save();
        return back()->with('success', 'product Added in the cart Successfully!');


    }





//        //cart
//        public function cart(){
//
//            $categories = Category::with('categories')->where(['parent_id'=> 0])->get();
//            $session_id = Session::get('session_id');
//
//            $userCart = Cart::where(['session_id' => $session_id])->get()->map(function($product){
//                    return Product::find($product->product_id);
//            });
////            dd($userCart);
////            $product = Product::where("id","=",$userCart->product_id)->get()->first();
////            dd($product);
//
//           return view('products.cart',[
//               'categories'=> $categories,
//                'userCart' => $userCart
//            ]);
//
//
//        }







    public function cart(Product $product){

        $categories = Category::with('categories')->where(['parent_id'=> 0])->get();
        $session_id = Session::get('session_id');
        $userCart = Cart::with('product')->where(['session_id' => $session_id])->get();
        // return  $userCart;
        //'product_id' => $product->id

        return view('products.cart',compact('userCart','categories','product'))->with('success', 'product Added in the cart Successfully!');

    }



    public function deletecart($id)
    {
        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        $cart=Cart::where('id',$id)->first();
        $cart->delete();
        return back()->with('success', 'product Deleted in the cart Successfully!');
    }



    public  function updateCartQuantity($id,$quantity)
    {
        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        $getCartDetails = Cart::with('product.product_attribute')->where('id',$id)->first();
        $session_id = Session::get('session_id');
        $get_cats_qty = Cart::where('session_id', $session_id)
            ->where('product_id',$getCartDetails->product_id)
            ->first();
        $prev_qty = $get_cats_qty->quantity;
        $new_aty_added = $prev_qty + $quantity;

        $qty_check = DB::table('product_attributes')->where('product_id',$getCartDetails->product_id)
            ->where('size',$getCartDetails->size)->first();
        if($qty_check->stock >= $new_aty_added){
            DB::table('carts')->where('id', $id)->increment('quantity', $quantity);
            return back()->with('success', 'product Quantity in the cart Updated Successfully!');
        }else{
            return back()->with('error', 'Required Product Quantity is not available!');
        }

    }





    // coupon code apply on add-cart  page

    public function apply_coupon(Request  $request)
    {
        Session::forget('CouponAmount');
        Session::forget('CouponCode');
        // $coupons= DB::table('coupons')->where('coupon_code', $request['coupon_code'])->count();
        $coupons = Coupon::where('coupon_code', $request['coupon_code'])->count();
        if($coupons==0){
            return back()->with('error', 'This coupon does not exists!');
        }

        else{

            $couponsactive = Coupon::where('coupon_code', $request['coupon_code'])->first();

            //Coupon Active
            if($couponsactive ->status==0)
            {
                return back()->with('error', 'Coupon is not Active!');
            }
            //Coupon Active End


            // Expiry-Date
            $expiry_date =  $couponsactive -> expiry_date;
            $current_date = date('Y-m-d');
            if($expiry_date < $current_date )
            {
                return back()->with('error', 'Coupon  Expired!');
            }
            //Expiry-Date  End



            // Check if amount type is Fixed or Percentage
            $session_id = Session::get('session_id');
            $get_cats_sesion = Cart::where('session_id', $session_id)->get();
            $total_amount =0 ;
            foreach($get_cats_sesion as $session)
            {
                $total_amount = $total_amount + ($session->price * $session->quantity);
            }
            if($couponsactive->amount_type=="Fixed"){
                $couponAmount = $couponsactive->amount;
            }else{
                $couponAmount = $total_amount * ($couponsactive->amount/100);
            }

            // Add Coupon Code & Amount in Session
            Session::put('CouponAmount',$couponAmount);
            Session::put('CouponCode',$request['coupon_code']);

            return redirect()->back()->with('success','Coupon code successfully
                applied. You are availing discount!');

        }




    }




    public function checkout()
    {
        $categories = Category::with('categories')->where(['parent_id' => 0])->get();
        $session_id = Session::get('session_id');
        DB::table('carts')->where(['session_id'=>$session_id])->update(['user_email'=>Auth::user()->email]);

        return view('products.checkout',compact('categories'));
    }




    public function checkoutproduct(Request $request)
    {

        // Prevent Duplicate Address End
        $count_shipping_address = DB::table('deliveries')->where(['user_id' => Auth::user()->id,'user_email'=>Auth::user()->email])->count();
        if($count_shipping_address > 0){
            Delivery::where('user_id',Auth::user()->id)->update([
                'name'=>$request['shipping_name'],
                'address'=>$request['shipping_address'],
                'city'=>$request['shipping_city'],
                'state'=>$request['shipping_state'],
                'pincode'=>$request['shipping_pincode'],
                'country'=>$request['shipping_country'],
                'mobile'=>$request['shipping_mobile']]);

            if (
                empty($request['shipping_address']) ||
                empty($request['shipping_city']) ||
                empty($request['shipping_state']) ||
                empty($request['shipping_pincode']) ||
                empty($request['shipping_country']) ||
                empty($request['shipping_mobile'])

            ) {
                return redirect()->back()->with('error', 'Please fill all fields to Checkout!');
            }

            //pincode
            $pincodeCount = DB::table('pincodes')->where('pincode',$request['shipping_pincode'])->count();
            if ($pincodeCount == 0) {
                return redirect()->back()->with('error', 'Your PostalCode/ZipCode location is not available for delivery. Please enter another PostalCode/ZipCode location.');
            }
            //pincode
            return redirect()->route('order_review')->
            with('success','Checkout Successfully');
        }else
        {

            $shipping = new Delivery();
            if (
                empty($request['shipping_address']) ||
                empty($request['shipping_city']) ||
                empty($request['shipping_state']) ||
                empty($request['shipping_pincode']) ||
                empty($request['shipping_country']) ||
                empty($request['shipping_mobile'])
            ) {
                return redirect()->back()->with('error', 'Please fill all fields to Checkout!');
            }

            $shipping->user_id = Auth::user()->id;
            $shipping->user_email = Auth::user()->email;
            $shipping->name = $request['shipping_name'];
            $shipping->address = $request['shipping_address'];
            $shipping->city = $request['shipping_city'];
            $shipping->state = $request['shipping_state'];
            $shipping->pincode = $request['shipping_pincode'];
            $shipping->country = $request['shipping_country'];
            $shipping->mobile = $request['shipping_mobile'];
            $shipping->save();






            return redirect()->route('order_review')->with('success','Checkout Successfully');


        }


    }


    public  function  order_review()
    {
        $categories = Category::with('categories')->where(['parent_id'=> 0])->get();
        $session_id = Session::get('session_id');
        $userCart = Cart::with('product')->where(['session_id' => $session_id])->get();
        //return $userCart;
        $shipping_address = Delivery::where('user_email',Auth::user()->email)->first();


        //pincode check COD and Paypal
        $codpincodeCount = DB::table('cod_pincodes')->where('pincode',$shipping_address->pincode)->count();
        $paypalpincodeCount = DB::table('prepaid_pincodes')->where('pincode',$shipping_address->pincode)->count();

        //pincode check COD and Paypal End



        //  Shipping Charges
        $total_weight = 0;
        foreach($userCart as $key => $product){
            $productDetails = Product::where('id',$product->product_id)->first();
            $total_weight = $total_weight + $productDetails->weight;
        }

        $shippingCharges = Product::getShippingCharges($total_weight,$shipping_address->country);
        Session::put('ShippingCharges',$shippingCharges);

        //shipping charges End


        $meta_title = "Order Review-E commerce website";
        return view('products.order_review',compact('categories','userCart','codpincodeCount','paypalpincodeCount','meta_title','shippingCharges'));
    }











    public  function  placeorder(Request $request)
    {

        $user_email = Auth::user()->email;

        if(empty(Session::get('CouponCode'))){
            $coupon_code = '';
        }else{
            $coupon_code = Session::get('CouponCode');
        }

        if(empty(Session::get('CouponAmount'))){
            $coupon_amount = '';
        }else{
            $coupon_amount = Session::get('CouponAmount');
        }



        // bug fix
        $usercarts = Cart::where('user_email',Auth::user()->email)->get();
        foreach ($usercarts as $usercart)
        {
            $cartquantity = $usercart->quantity;
            $productid = $usercart->product_id;
            $sku = $usercart->sku;

            $getStock = Product_attribute::where('sku',$usercart->sku)->get();
            foreach ($getStock as $stock)
            {
                $attributestock= $stock->stock;
                $attributes_productid= $stock->product_id;

            }


            if($attributestock==0)
            {

                DB::table('carts')->where(['product_id'=>$attributes_productid,'user_email'=>$user_email])->delete();
                return back()->with('error','Sold Out! product removed from Cart. Try again!');
            }

            if($cartquantity > $attributestock )
            {
                DB::table('carts')->where(['product_id'=>$attributes_productid,'user_email'=>$user_email])->delete();
                return back()->with('error','Reduce Product Stock! please Try Again');
            }


            //product activate or not

            $getproduct_status = Product::select('status')->where(['id'=>$usercart->product_id])->first();
            $a = $getproduct_status->status;
            if($a == 0){
                DB::table('carts')->where(['product_id'=>$attributes_productid,'user_email'=>$user_email])->delete();
                return back()->with('error','Product status not true! please Try Again');
            }
            //product activate or not End


            //Category Active or not
            $getCategoryId = Product::select('category_id')->where(['id'=>$usercart->product_id])->first();
            $cat_id = $getCategoryId->category_id;
            $getCategoryStatus = Category::select('status')->where(['id'=>$cat_id])->first();
            $cat_status= $getCategoryStatus->status;
            if($cat_status==0){
                DB::table('carts')->where(['product_id'=>$attributes_productid,'user_email'=>$user_email])->delete();
                return back()->with('error','category Not active! please Try Again');
            }
            //Category Active or not  End


            //Delete Attribute,If Admin Delete Attribute Start

            $getAttributeCount = Product_attribute::where(['product_id'=>$usercart->product_id,'size'=>$usercart->size])->count();
            $count = $getAttributeCount;
            if($count==0){
                DB::table('carts')->where(['product_id'=>$attributes_productid,'user_email'=>$user_email])->delete();
                return back()->with('error','Attribute Deleted! please Try Again');
            }
             //Delete Attribute,If Admin Delete Attribute Start  End

        }

     //bug fix End




        $shipping_address = Delivery::where('user_email',Auth::user()->email)->first();

        //pincode
        $pincodeCount = DB::table('pincodes')->where('pincode',$shipping_address->pincode)->count();
        if ($pincodeCount == 0) {
            return redirect()->back()->with('error', 'Your PostalCode/ZipCode location is not available for delivery. Please enter another PostalCode/ZipCode location.');
        }
        //pincode

        $placeorders                   = new Order();
        $placeorders -> user_id        = $shipping_address->user_id;
       // dd( $placeorders -> user_id );
        $placeorders -> user_email     = $shipping_address->user_email;
        $placeorders -> name           = $shipping_address->name;
        $placeorders -> address        = $shipping_address->address;
        $placeorders -> city           = $shipping_address->city;
        $placeorders -> state          = $shipping_address->state;
        $placeorders -> country        = $shipping_address->country;
        $placeorders -> pincode        = $shipping_address->pincode;
        $placeorders -> mobile         = $shipping_address->mobile;
        $placeorders ->	order_status   = "New";
        $placeorders -> coupon_code    = $coupon_code;
        $placeorders -> coupon_amount  = $coupon_amount;
        $placeorders -> payment_method = $request['payment_method'];
        $placeorders ->shipping_charges = Session::get('ShippingCharges');
        $placeorders -> grand_total    = $request['grand_total'];
        $placeorders -> save();

        $order_id     =     DB::getPdo()->lastInsertId();
        $cartProducts = DB::table('carts')->where(['user_email'=>Auth::user()->email])->get();

        foreach($cartProducts as $pro){
            $cartPro                 = new Order_product();
            $cartPro->order_id       = $order_id;
            $cartPro->user_id        = Auth::user()->id;
            $cartPro->product_id     = $pro->product_id;
            $cartPro->product_code   = $pro->product_code;
            $cartPro->product_name   = $pro->product_name;
            $cartPro->product_color  = $pro->product_color;
            $cartPro->product_size   = $pro->size;
            $cartPro->product_price  = $pro->price;
            $cartPro->product_qty    = $pro->quantity;
            $cartPro->save();
            //return back();


            //Product_Attribute stock decrease after  order replacement
            // dd($pro);
            $getProductStock = Product_attribute::where('sku',$pro->sku)->first();
           //  dd ($getProductStock);
            $newStock = $getProductStock->stock - $pro->quantity;
            if($newStock<0){
                $newStock = 0;
            }
            Product_attribute::where('sku',$pro->sku)->update(['stock'=>$newStock]);

            //Product_Attribute stock decrease after  order replacement End




        }


        //Thanks page for COD
        Session::put('order_id', $order_id);
        Session::put('grand_total', $request['grand_total']);
        $categories = Category::with('categories')->where(['parent_id'=> 0])->get();
        if ($request['payment_method'] == "COD") {
        $user_email = Auth::user()->email;

        //send Email To the user
            $user_id= Auth::user()->id;
            $productDetails = Order::with('order_products')->where('id',$order_id)->get();
            $userBillDetails = Profile::with('user')->where('user_id',$user_id)->first();
            $messageData = [
                'email' => $user_email,
                'name' =>$shipping_address->name,
                'order_id' =>$order_id,
                'productDetails'=>$productDetails,
                'userBillDetails'=>$userBillDetails,
            ];
            Mail::send('emails.order', $messageData, function ($message) use ($user_email) {
                $message->to($user_email)->subject('Order Placed - E-com Website');
            });
        //send Email To the user End

        DB::table('carts')->where('user_email',$user_email)->delete();
        return view('orders.thanks',compact('categories'));
        } else {

            // Paypal page start
            return redirect('product/user/paypal');
            // Paypal page End
        }
        //Thanks Page for COD END





    }



       public  function  userOrders()
       {
           $user_email = Auth::user()->email;
           $categories = Category::with('categories')->where(['parent_id'=> 0])->get();
           $orders = Order::with('order_products')->where('user_email',$user_email)->orderBy('id','DESC')->get();
           return view('orders.user_orders',compact('categories','orders'));
       }




       public  function userOrderDetails($id)
       {
           $categories = Category::with('categories')->where(['parent_id'=> 0])->get();
           $ordersDetails = Order_product::where('id',$id)->first();
           return view('orders.user_order_details',compact('categories','ordersDetails'));

       }







       //Paypal
       public  function paypal()
       {

           $user_email = Auth::user()->email;
           DB::table('carts')->where('user_email',$user_email)->delete();
           $categories = Category::with('categories')->where(['parent_id'=> 0])->get();
           return view('orders.paypal',compact('categories'));
       }



    //thanks paypal
    public function thanksPaypal(){
        $categories = Category::with('categories')->where(['parent_id'=> 0])->get();
        return view('orders.thanks_paypal',compact('categories'));
    }


    //cancel paypal
    public function cancelPaypal(){
        $categories = Category::with('categories')->where(['parent_id'=> 0])->get();
        return view('orders.cancel_paypal',compact('categories'));
    }




    public function search(Request $request){

        $categories = Category::with('categories')->where(['parent_id'=> 0])->get();
        $search_product = $request['product_name'];

            $productsAll = Product::where(function ($query) use ($search_product) {
                $query->where('product_name', 'like', '%' . $search_product . '%')
                    ->orWhere('product_code', 'like', '%' . $search_product . '%')
                    ->orWhere('description', 'like', '%' . $search_product . '%')
                    ->orWhere('product_color', 'like', '%' . $search_product . '%');
            })->where('status', 1)->paginate(12);

            If( $productsAll->count())
            {
                return view('index', compact('productsAll', 'categories'));
            }
            else
             {
                     return view('error', compact('categories'));
             }




    }


    //cms page

    public  function cmsPage($url)
    {
        $categories = Category::with('categories')->where(['parent_id'=> 0])->get();
        $cmsPageCount = Cms::where(['url'=>$url,'status'=>1])->count();
        if($cmsPageCount > 0){
            $cmsPageDetails = Cms::where(['url'=>$url,'status'=>1])->first();
            //Meta tags
            $meta_title       = $cmsPageDetails->meta_title;
            $meta_description = $cmsPageDetails->meta_description;
            $meta_keywords    = $cmsPageDetails->meta_keywords;
            //Meta tags end
        }else{
            return view('error', compact('categories'));
        }

        return view('pages.cms_page')->with(compact('cmsPageDetails','categories','meta_title','meta_description','meta_keywords'));

    }


    //Contact us

    public function contact_us()
    {
        $categories = Category::with('categories')->where(['parent_id'=> 0])->get();

        // Meta tags
        $meta_title = "Contact-us E-shop Sample Website";
        $meta_description = "If you have any query to related our product,then contact us";
        $meta_keywords = "eshop website, online shopping, men clothing";
        return view('contact.contact_us',compact('categories','meta_title','meta_description','meta_keywords'));
    }


    public function send_contact_info(Request $request)
    {
        //validation
        $this->validate($request,[
            'name'         => 'required|string|regex:/^[\pL\s\-]+$/u|max:255',
            'email'        => 'required|email',
            'subject'      => 'required|string',
            'message'      => 'required|string',
        ]);
        //validation End

        // Send Contact Email
        $email = "tareq@gmail.com";
        $messageData = [
            'name'    =>$request['name'],
            'email'   =>$request['email'],
            'subject' =>$request['subject'],
            'comment' =>$request['message']
        ];
        Mail::send('emails.enquiry',$messageData,function($message)use($email){
            $message->to($email)->subject('Enquiry from E-com Website');
        });

        return redirect()->back()->with('success','Thanks for your enquiry. We will get back to you soon.');

    }



    //check pincode
    public function checkPincode(Request $request)
    {
        if($request->isMethod('post')){
            $data = $request->all();
            echo $pincodeCount = DB::table('pincodes')->where('pincode',$data['pincode'])->count();
        }
    }


    //Filter
















}
