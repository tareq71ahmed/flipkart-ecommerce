<?php

namespace App\Http\Controllers;
use App\Coupon ;
use Illuminate\Http\Request;

class CouponController extends Controller
{

    public function coupon()
    {
        return view('admin.coupons.add_coupon');
    }


    public function add_coupon(Request $request)
    {
        $coupons = new Coupon();
        $coupons->coupon_code=$request['coupon_code'];
        $coupons->amount=$request['amount'];
        $coupons->amount_type=$request['amount_type'];
        $coupons->expiry_date=$request['expiry_date'];
        $coupons->status=$request['status']?? " ";
        $coupons->save();
        return redirect()-> route('view_coupon')->with('success','Successfully! Coupon Added');


    }


    public function view_coupon()
    {
        $coupons =  Coupon::all();
        return view('admin.coupons.view_coupon',compact('coupons'));
    }



    public function edit_coupon($id)
    {
        $coupon = Coupon::where(['id'=>$id])->first();
        return view('admin.coupons.update_coupon',compact('coupon'));
    }

    public function update_coupon(Request $request,$id)
    {
        $coupon = Coupon::find($id);
        $coupon->coupon_code=$request['coupon_code'];
        $coupon->amount=$request['amount'];
        $coupon->amount_type=$request['amount_type'];
        $coupon->expiry_date=$request['expiry_date'];
        $coupon->status=$request['status']?? "0";
        $coupon->save();
        return redirect()->back()->with('success', 'Coupon has been Updated successfully');
    }



    public function delete_coupon($id){
        Coupon::where(['id'=>$id])->delete();
       return back()->with('error', 'Coupon has been deleted successfully');
    }




}
