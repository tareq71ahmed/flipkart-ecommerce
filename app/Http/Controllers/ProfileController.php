<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profile;
class ProfileController extends Controller
{

    //account
    public function account(Product $product)
    {

        $categories = Category::with('categories')->where(['parent_id' => 0, 'status' => 1])->get();
        return view('users.account',compact('categories','product'));
    }

    public function Profile(Request  $request)
    {

        $profile = Profile::where('user_id',Auth::id())->first();
        $profile->address = $request['address']?? "";
        $profile->city = $request['city']?? "";
        $profile->state = $request['state']?? "";
        $profile->country = $request['country']?? "";
        $profile->pincode = $request['pincode']?? "";
        $profile->mobile = $request['mobile']?? "";
        $profile->save();

      return redirect()->back()->with('success','Your Profile  Updated Successfully');
    }



}
