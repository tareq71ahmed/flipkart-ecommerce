<?php

namespace App\Http\Controllers;



use App\Admin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Session;
use Session;
use DB;

class AdminController extends Controller
{

    //    admin registration
//    public function adminregistration(){
//
//        $admin_count = Admin::Where(['email'=>request('email')])->count();
//
//        if ($admin_count > 0) {
//            return redirect()->back()->with('error', ' Email Already Exists !');
//        } else {
//
//            Admin::create([
//                'name' => request('name'),
//                'email' => request('email'),
//                'user_type' => request('user_type'),
//                'password' => Hash::make(request('password')),
//            ]);
//        }
//         return redirect()->to('admin/login')->with('error','For verify Account ! ps Contact to SuperAdmin!');
//        //return redirect()->back();
//
//    }


    //    admin loginfrom
     public function loginform(){
         return view('admin.admin_login');

     }


     //admin login
    public function adminLogin(Request $request){

        if ($request->ismethod('post')) {
            $data = $request->input();

            if ( Auth::guard('admin')->attempt(['email' => $data['email'], 'password' => $data['password'],
                 'status' => '1'])){

             //  Session::put('adminSession', $data['email']);

                return view('admin.dashboard');
            }
            else {

                return back()->with('error',' Invalid UserName Or Password');
            }

        }

    }





      //admin dashboard
      public function dashboard()
      {
//          $adminDetails = Admin::where(['email'=>Session::get('adminSession')])->first();
          return view('admin.dashboard');
      }



      //admin settings
      public function settings(){
//          $adminDetails = Admin::where(['email'=>Session::get('adminSession')])->first();

          return view('admin.settings');
      }

     //admin check pasword
    public function chkPassword(Request $request){
        $data = $request->all();

        $current_password = $data['current_pwd'];
        $check_password = Admin::where(['status'=>'1','email' => Auth::guard('admin')->user()->email])->first();
        if(Hash::check($current_password,$check_password->password)){
            echo "true"; die;
        }else {
            echo "false"; die;
        }


    }



     //update password
    public function updatePassword(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            //echo "<pre>"; print_r($data); die;
            $check_password = Admin::where(['email' => Auth::guard('admin')->user()->email,'status'=>'1'])->first();
            $current_password = $data['current_pwd'];
            if(Hash::check($current_password,$check_password->password)){
                $password = bcrypt($data['new_pwd']);
                Admin::where(['status'=>'1','email' =>Auth::guard('admin')->user()->email ])->update(['password'=>$password]);
                return redirect('/admin/settings')->with('success','Password updated Successfully!');
            }else {
                return redirect('/admin/settings')->with('error','Incorrect Current Password!');
            }



        }
    }





//    public function updatePassword(Request $request){
//        if($request->isMethod('post')){
//            $data = $request->all();
//            //echo "<pre>"; print_r($data); die;
//            $adminCount = Admin::where(['email' => Session::get('adminSession'),'password'=>md5($data['current_pwd'])])->count();
//
//            if ($adminCount == 1) {
//                // here you know data is valid
//                $password = md5($data['new_pwd']);
//                Admin::where('email',Session::get('adminSession'))->update(['password'=>$password]);
//                return redirect('/admin/settings')->with('success', 'Password updated successfully.');
//            }else{
//                return redirect('/admin/settings')->with('error', 'Current Password entered is incorrect.');
//            }
//
//
//        }
//    }





    //logout
    public function  logout(){

        Auth::guard('admin')->logout();

        return redirect('admin/login')->with('error','You Logout successfully');
    }



    // Admin / subAdmin

    public  function  addAdmin()
    {
        return view('admin.admin_sub_admin.add_admin');
    }




    public  function  viewAdmin()
    {

        $admins = Admin::get();
        return view('admin.admin_sub_admin.view_admins_sub_admins',compact('admins'));

    }



    public function  add_Admin_subadmin(Request $request)
    {

        $admin_count = Admin::Where(['email'=>request('email')])->count();

        if ($admin_count > 0) {
            return redirect()->back()->with('error', ' Email Already Exists !');
        } else {


            if (empty($request['user_type'])) {

                return redirect()->back()->with('error', 'ps select User type');
            }

            if($request['user_type'] == 'admin'){
            $admin = New Admin();
            $admin -> name=$request['name'];
            $admin -> email=$request['email'];
            $admin -> user_type=$request['user_type'];
            $admin -> status=$request['status'] ??" ";
            $admin -> password=Hash::make(request('password'));
            $admin -> save();
            return back()->with('success',' Successfull Admin!');
            }
            elseif ($request['user_type'] == 'sub admin')
            {
                $admin = New Admin();
                $admin -> name=$request['name'];
                $admin -> email=$request['email'];
                $admin -> user_type= $request['user_type'] ;
                $admin -> status=$request['status']??" ";
                $admin -> password=Hash::make(request('password'));
                $admin -> categories_access=$request['categories_access'] ?? " ";
                $admin -> products_access=$request['products_access'] ?? " ";
                $admin -> orders_access=$request['orders_access'] ?? " ";
                $admin -> users_access=$request['users_access'] ?? " ";
                $admin -> save();
                return back()->with('success',' Successfull Added subAdmin!');

           }



        }




    }





    public  function  editAdmin_sub($id)
    {
        $admin =Admin::find($id);
        $admin_status = Admin::where(['id'=>$id])->first();
        return view('admin.admin_sub_admin.edit',compact('admin','admin_status'));
    }


    public  function updateAdmin_sub(Request $request,$id)
    {

//        $adminDetails = Admin::where(['email' => $request['email']])->get()->first();
//        return  $adminDetails ;

        if($request['user_type'] == 'admin'){
            $admin  =    Admin::where('id',$id)->first();
            $admin -> name=$request['name'];
            $admin -> email=$request['email'];
            $admin -> status=$request['status'];
            $admin -> password=Hash::make(request('password'));
            $admin -> update();
            return redirect()->route('viewAdmin')->
            with('success','Admin Updated Successfully');
           // return back()->with('success',' Successfull Admin!');
        }

        elseif ($request['user_type'] == 'sub admin')

        {

            $admin  =    Admin::where('id',$id)->first();
            $admin -> name=$request['name'];
            $admin -> email=$request['email'];
            $admin -> status=$request['status'] ??" ";
            $admin -> password=Hash::make(request('password'));
            $admin -> categories_access=$request['categories_access'] ?? " ";
            $admin -> products_access=$request['products_access'] ?? " ";
            $admin -> orders_access=$request['orders_access'] ?? " ";
            $admin -> users_access=$request['users_access'] ?? " ";
            $admin -> update();
            return redirect()->route('viewAdmin')->
            with('success','Sub Admin Updated Successfully');
           // return back()->with('success',' Successfull Added subAdmin!');

        }



    }






}
