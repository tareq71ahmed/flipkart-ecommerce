<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded=[];

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function getBodyHtmlAttribute(){
        return \Parsedown::instance()->text($this->description);
    }

    public function product_attribute()
    {
        return $this->hasMany('App\Product_attribute');
    }



    //One To Many

    public function images(){
        return $this->hasMany('App\Product_image');

    }


    // slug
    public function setProductNameAttribute($value)
    {
        $this->attributes['product_name'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }


    public function getUrlAttribute($value)
    {
        return route('productsdetails',$this->slug);
    }



    public static function getShippingCharges($total_weight,$country){
        $shippingDetails = ShippingCharge::where('country',$country)->first();
        if($total_weight>0){
            if($total_weight > 0 && $total_weight<=500){
                $shipping_charges = $shippingDetails->shipping_charges0_500g;
            }else if($total_weight>=501 && $total_weight<=1000){
                $shipping_charges = $shippingDetails->shipping_charges501_1000g;
            }else if($total_weight>=1001 && $total_weight<=2000){
                $shipping_charges = $shippingDetails->shipping_charges1001_2000g;
            }else if($total_weight>=2001 && $total_weight<=5000){
                $shipping_charges = $shippingDetails->shipping_charges2001_5000g;
            }else{
                $shipping_charges = 0;
            }
        }else{
            $shipping_charges = 0;
        }
        return $shipping_charges;
    }






}
