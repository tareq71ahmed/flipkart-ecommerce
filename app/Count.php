<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Session;

class Count extends Model
{


    public static function cartCount()
    {
        if (Auth::check()) {
            $cartCount = Cart::where(['user_email' => Auth::user()->email])->sum('quantity');
            return $cartCount;

        } else {

            $session_id = Session::get('session_id');
            $cartCount = Cart::where(['session_id' => $session_id])->sum('quantity');

        }

        return $cartCount;

    }



    public static  function CategoryCount($category_id)
    {
        $CategoryCount = Product::where(['category_id'=>$category_id,'status' => 1])->count();
        return $CategoryCount;
    }




}
