
<?php

    /*
    |--------------------------------------------------------------------------
    | Web Routes
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    | routes are loaded by the RouteServiceProvider within a group which
    | contains the "web" middleware group. Now create something great!
    |
    */
    //
    // Route::get('/', function () {
    //    return view('welcome');
    // });

    Auth::routes();

    //Route::get('/home', 'HomeController@index')->name('home');

    //Check button , jquery.uniform.js->654 line,diseble

    /*
    |--------------------------------------------------------------------------
    | Admin Register,Login,Admin Dashboard part
    |--------------------------------------------------------------------------
    */
    //Admin Register
//    Route::view('admin/register','auth.admin-register');
//    Route::post('admin/registration','AdminController@adminregistration')->name('admin.registration');

    //admin Login
    Route::get('admin/login','AdminController@loginform');
    Route::post('admin/dashboard','AdminController@adminLogin')->name('admin.login');

    //Admin Dashboard after login
    Route::group(['middleware'=>['CheckRole']], function (){
    Route::get('admin/dashboard','AdminController@dashboard')->name('admin.dashboard');
    Route::get('admin/settings','AdminController@settings')->name('admin.settings');
    Route::get('/admin/check-pwd','AdminController@chkPassword');
    Route::post('/admin/update-pwd','AdminController@updatePassword');
    // Admin Category
    Route::get('admin/add-category','CategoryController@category')->name('category');
    Route::post('admin/add-category','CategoryController@addcategory')->name('add.category');
    Route::get('admin/view-category','CategoryController@viewcategory')->name('view.category');
    Route::get('admin/{id}/edit-category','CategoryController@editcategory')->name('edit.category');
    Route::post('admin/{id}/update-category','CategoryController@updatecategory')->name('update.category');
    Route::get('admin/{id}/delete-category','CategoryController@deletecategory')->name('delete.category');

    //Products
    Route::get('admin/add-product','ProductsController@product')->name('product');
    Route::post('admin/add-product','ProductsController@addproduct')->name('add.product');
    Route::get('admin/view-product','ProductsController@viewproduct')->name('view.product');
    Route::get('admin/{id}/edit-product','ProductsController@editproduct')->name('edit.product');
    Route::post('admin/{id}/update-product','ProductsController@updateproduct')->name('update.product');
    Route::get('admin/{id}/delete-product','ProductsController@deleteproduct')->name('delete.product');
    Route::get('/admin/delete-product-image/{id}','ProductsController@deleteProductImage')->name('deleteimage');
    Route::get('/admin/delete-product-video/{id}','ProductsController@deleteProductVideo')->name('delete_video');


    //Products Attribute
    Route::get('admin/{id}/attribute','ProductsController@product_attribute')->name('product_attribute');
    Route::post('admin/{id}/{product_code}/add_attribute','ProductsAttributeController@product_store_attribute')->name('product_store_attribute');
    Route::get('admin/view/attribute','ProductsAttributeController@product_view_attribute')->name('product_view_attribute');
    Route::get('/admin/{id}/delete-attribute','ProductsAttributeController@deleteattribute')->name('deleteattribute');
    Route::post('/admin/{id}/update-attribute','ProductsAttributeController@updateattribute')->name('updateattribute');

    //Products Multiple Image Add
    Route::get('admin/{id}/multiple-images','ProductsController@add_multiple_images')->name('add_multiple_images');
    Route::post('admin/{id}/multiple-images','Product_image_Controller@image_store')->name('image_store');
    Route::get('admin/view/images','Product_image_Controller@Product_view_images')->name('product_view_images');
    Route::get('admin/{id}/delete-image','Product_image_Controller@delete_image')->name('delete_image');


     //Coupon
    Route::get('coupon/cart/coupon','CouponController@coupon')->name('coupon');
    Route::post('coupon/cart/coupon','CouponController@add_coupon')->name('add_coupon');
    Route::get('coupon/cart/view-coupon','CouponController@view_coupon')->name('view_coupon');
    Route::get('coupon/cart/{id}/edit-coupon','CouponController@edit_coupon')->name('edit_coupon');
    Route::post('coupon/cart/{id}/update-coupon','CouponController@update_coupon')->name('update_coupon');
    Route::get('coupon/cart/{id}/delete-coupon','CouponController@delete_coupon')->name('delete_coupon');


    // View User Order Product

    Route::get('/admin/user/view-orders','UserOrderProductController@viewOrders')->name('viewOrders');
    Route::get('/admin/user/view-order/{order_id}','UserOrderProductController@viewOrderDetails')->name('viewOrderDetails');
    Route::post('/admin/user/update-order-status','UserOrderProductController@updateOrderStatus')->name('updateOrderStatus');
    Route::get('/admin/user/view-order-invoice/{order_id}','UserOrderProductController@viewOrderinvoice')->name('viewOrderinvoice');

    // View All User
    Route::get('/admin/user/all-user','registeredUser@all_user')->name('all_user');


    //cms page
    Route::get('admin/add/cms','CmspageController@cms')->name('cms');
    Route::post('admin/add/cms','CmspageController@add_cms')->name('add_cms');
    Route::get('admin/view/cms','CmspageController@view_cms')->name('view_cms');
    Route::get('admin/{id}/cms','CmspageController@edit_cms')->name('edit_cms');
    Route::post('admin/{id}/update-cms','CmspageController@update_cms')->name('update_cms');
    Route::get('admin/{id}/delete-cms','CmspageController@delete_cms')->name('delete_cms');

    //shipping page
    Route::get('admin/view-shipping','ShippingController@shipping')->name('shipping');
    Route::get('admin/{id}/view-shipping','ShippingController@edit_shipping')->name('edit_shipping');
    Route::post('admin/{id}/update-shipping','ShippingController@update_shipping')->name('update_shipping');


    // Admin -> sub Admin
    Route::get('admin/add-admin','AdminController@addAdmin')->name('addAdmin')  ;
    Route::post('admin/add-admin','AdminController@add_Admin_subadmin')->name('add_Admin_subadmin');
    Route::get('admin/view-admin-subadmin','AdminController@viewAdmin')->name('viewAdmin');
    Route::get('admin/edit/{id}/admin-sub-admin','AdminController@editAdmin_sub')->name('editAdmin_sub');
    Route::post('admin/update/{id}/admin-sub-admin','AdminController@updateAdmin_sub')->name('updateAdmin_sub');




    //Admin Logout
    Route::get('/admin-logout', 'AdminController@logout')->name('admin.logout');
    });







    /*
     |--------------------------------------------------------------------------
     | frontend  part
     |--------------------------------------------------------------------------
     */

    Route::get('/','IndexController@index')->name('home');

    //search product
    Route::get('/search/product','IndexController@search')->name('search');

    //Listing Page
    Route::get('/{url}','IndexController@listing')->name('listing');

    //products Details
    Route::get('product/{slug}','IndexController@productsdetails')->name('productsdetails');

    //Display cms page on Frontend
    //apply on front_footer.blade.php
    Route::match(['get','post'],'cms/page/{url}','IndexController@cmsPage');

    //Contact Us Page
    Route::get('page/contact-us','IndexController@contact_us')->name('contact_us');
    Route::post('page/contact-us','IndexController@send_contact_info')->name('send_contact_info');

    //filter












    /*
     |--------------------------------------------------------------------------
     | Cart
     |--------------------------------------------------------------------------
     */

    Route::post('product/add-cart','IndexController@addtocart')->name('addtocart'); //form
    Route::get('product/cart/view-cart','IndexController@cart')->name('cart'); //view
    //Update Quantity in cart
    Route::get('/cart/update-quantity/{product_id}/{quantity}','IndexController@updateCartQuantity');
    //Delete cart
    Route::get('product/cart/view-cart/{id}','IndexController@deletecart')->name('deletecart');
    //coupon code apply on add-cart  page
    Route::post('product/cart/apply-coupon','IndexController@apply_coupon')->name('apply_coupon');

    // Check Pincode
    Route::post('/check-pincode','IndexController@checkPincode');
    //Product Price
    Route::post('product/price/display_on_size','IndexController@price_display_on_size')->name('price_display_on_size');


    /*
    |--------------------------------------------------------------------------
    | User Registration,Login,Update-password,User Account
    |--------------------------------------------------------------------------
    */
   Route::get('login','UserController@login')->name('login');
   Route::get('register','UserController@register')->name('register');
   Route::post('user/registration','UserController@userregistration')->name('registration');
   Route::post('/','UserController@userLogin');

   //Email
    Route::get('confirm/{code}','UserController@confirmAccount');
   //Forgot Password
    Route::match(['get','post'],'user/password/forgot-password','UserController@forgotPassword');

    //coupon code apply on add-cart  page
    Route::post('product/cart/apply-coupon','IndexController@apply_coupon')->name('apply_coupon');

    //Display cms page on Frontend
    //apply on front_footer.blade.php
    Route::match(['get','post'],'cms/page/{url}','IndexController@cmsPage');
    //Contact Us Page
    Route::get('page/contact-us','IndexController@contact_us')->name('contact_us');
    Route::post('page/contact-us','IndexController@send_contact_info')->name('send_contact_info');
    // Check Pincode
    Route::post('/check-pincode','IndexController@checkPincode');


  //User Account
   Route::group(['middleware'=>['Frontlogin']], function (){
   Route::get('/user/logout', 'UserController@logout')->name('user.logout');
   Route::get('user/account', 'ProfileController@account')->name('account');
   Route::post('user/profile', 'ProfileController@profile')->name('profile');
   //password
   Route::get('/user/check-pwd','UserController@chkPassword');
   Route::post('/user/update-pwd','UserController@updatePassword')->name('uppwd');
   //checkout
   Route::get('product/checkout/billing-shipping-address','IndexController@checkout')->name('checkout');
   Route::post('product/checkout/product/billing-shipping-address','IndexController@checkoutproduct')->name('checkoutproduct');
    //order Review
   Route::get('product/checkout/order-review','IndexController@order_review')->name('order_review');
     // Place Order / order-replacement
   Route::post('product/place-order','IndexController@placeorder')->name('placeorder');


       // Thanks Page
       Route::get('product/thanks','IndexController@thanks')->name('thanks');

       // Users Orders Page
       Route::get('product/user/orders','IndexController@userOrders')->name('user_orders_product');
       // User Ordered Products Details
       Route::get('product/user/orders/{id}','IndexController@userOrderDetails')->name('userOrderDetails');
       // Paypal Page
       Route::get('product/user/paypal','IndexController@paypal')->name('paypal');
       //Paypal Thanks Page
       Route::get('/paypal/user/thanks','IndexController@thanksPaypal');

       //Paypal Cancel Page
       Route::get('/paypal/user/cancel','IndexController@cancelPaypal');



    });






















