@extends('layouts.frontLayout.front_design')
@section('content')

    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                   <li><a href="{{route('user_orders_product')}}">Orders</a></li>
{{--                  <li class="active">{{ $ordersDetails->id }}</li>--}}
                </ol>
            </div>
        </div>
    </section>

    <section id="do_action">
        <div class="container">
            <div class="heading" align="center">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                    <tr>
                        <th>Product Code</th>
                        <th>Product Name</th>
                        <th>Product Size</th>
                        <th>Product Color</th>
                        <th>Product Price</th>
                        <th>Product Qty</th>
                    </tr>
                    </thead>
                    <tbody>

                        <tr>
                            <td>{{ $ordersDetails->product_code }}</td>
                            <td>{{ $ordersDetails->product_name }}</td>
                            <td>{{ $ordersDetails->product_size }}</td>
                            <td>{{ $ordersDetails->product_color }}</td>
                            <td>$ {{ $ordersDetails->product_price }} | (BDT {{ $ordersDetails->product_price*80 }})</td>
                            <td>{{ $ordersDetails->product_qty }}</td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
    </section>



@endsection
