@extends('layouts.frontLayout.front_design')
@section('content')

    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Orders</li>
                </ol>
            </div>
        </div>
    </section>

    <section id="do_action">
        <div class="container">
            <div class="heading" align="center">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                    <tr>
                        <th>Order ID</th>
                        <th>Ordered Products</th>
                        <th>Payment Method</th>
                        <th>Grand Total</th>
                        <th>Created on</th>


                    </tr>
                    </thead>
                    <tbody>

                    @foreach($orders as $order)
                        <tr>
                            <td>{{$order->id}}</td>
                            <td>
                                @foreach($order->order_products as  $order_product)
                                   <h5>Product Code:</h5><a href="{{route('userOrderDetails',$order_product->id)}}">{{$order_product->product_code}}</a>
                                    @endforeach
                            </td>
                            <td>{{$order->payment_method}}</td>
                            <td>$ {{$order->grand_total}} | (BDT {{$order->grand_total*80}})</td>
                            <td>{{$order->created_at}}</td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </section>

@endsection
