@php
    $adminDetails = \App\Admin::where(['email' =>Auth::guard('admin')->user()->email] )->first();
@endphp



<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
    <ul>
        <li ><a href="{{route('admin.dashboard')}}"><i class="icon icon-home"></i> <span>Dashboard</span></a>
        </li>



        @if($adminDetails->categories_access ==  1 )
        <li class="submenu"><a href="#"><i class="icon icon-th-list"></i> <span>Category</span> <span
                    class="label label-important">2</span></a>
            <ul>
                <li>
                    <a href="{{route('category')}}">Add Cayegory</a>
                </li>
                <li>
                    <a href="{{route('view.category')}}">View Category</a>
                </li>

            </ul>
        </li>
       @endif


{{--        @if(Session::get('adminDetails')['products_access'] == 1 )--}}
        <li class="submenu"><a href="#"><i class="icon icon-th-list"></i> <span>Product</span> <span
                    class="label label-important">2</span></a>
            <ul>
                <li>
                    <a href="{{route('product')}}">Add Product</a>
                </li>
                <li>
                    <a href="{{route('view.product')}}">View Product</a>
                </li>


            </ul>
        </li>
{{--        @endif--}}

{{--        @if(Session::get('adminDetails')['user_type'] == "admin" )--}}
        <li class="submenu"><a href="#"><i class="icon icon-th-list"></i> <span>Coupon</span> <span
                    class="label label-important">2</span></a>
            <ul>
                <li>
                    <a href="{{route('coupon')}}">Add Coupon</a>
                </li>
                <li>
                    <a href="{{route('view_coupon')}}">view Coupon</a>
                </li>

            </ul>
        </li>
{{--        @endif--}}


{{--        @if(Session::get('adminDetails')['orders_access'] == 1 )--}}
        <li class="submenu"><a href="#"><i class="icon icon-th-list"></i> <span>Orders Product</span> <span
                    class="label label-important">1</span></a>
            <ul>
                <li><a href="{{route('viewOrders')}}">View Order</a></li>
            </ul>
        </li>
{{--        @endif--}}

{{--        @if(Session::get('adminDetails')['users_access'] == 1 )--}}

        <li class="submenu"><a href="#"><i class="icon icon-th-list"></i> <span>Registered User</span> <span
                    class="label label-important">1</span></a>
            <ul>
                <li><a href="{{route('all_user')}}">All User</a></li>
            </ul>
        </li>

{{--        @endif--}}
        <li class="submenu"><a href="#"><i class="icon icon-th-list"></i> <span>CMS page</span> <span
                    class="label label-important">2</span></a>
            <ul>
                <li><a href="{{route('cms')}}">Add CMS</a></li>
                <li><a href="{{route('view_cms')}}">view CMS</a></li>

            </ul>
        </li>


        <li class="submenu"><a href="#"><i class="icon icon-th-list"></i> <span>Shipping</span> <span
                    class="label label-important">1</span></a>
            <ul>
                <li><a href="{{route('shipping')}}">View Shipping</a></li>


            </ul>
        </li>


{{--        @if(Session::get('adminDetails')['user_type'] == "admin" )--}}
        <li class="submenu"><a href="#"><i class="icon icon-th-list"></i> <span>Admin/subAdmin</span> <span
                    class="label label-important">2</span></a>
            <ul>

                <li><a href="{{route('addAdmin')}}">Add Admin/subAdmin</a></li>
                <li><a href="{{route('viewAdmin')}}">View Admin/subAdmin</a></li>

            </ul>
        </li>

{{--        @endif--}}




    </ul>
</div>
