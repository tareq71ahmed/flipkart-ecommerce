<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>@if(!empty($meta_title)){{ $meta_title }} @else Home | E-Shopper @endif</title>
    @if(!empty($meta_description))<meta name="description" content="{{ $meta_description }}">@endif
    @if(!empty($meta_keywords))<meta name="keywords" content="{{ $meta_keywords }}">@endif


    <link href="{{ asset('css/frontend_css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend_css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend_css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend_css/price-range.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend_css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend_css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend_css/responsive.css') }}" rel="stylesheet">
    <script src="{{ asset('js/frontend_js/jquery.js') }}"></script>
    <link href="{{ asset('css/frontend_css/easyzoom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/frontend_css/passtrength.css') }}" rel="stylesheet">













    <!--   <script src="js/html5shiv.js"></script> -->
    <!--   <script src="js/respond.min.js"></script> -->





    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">







</head>

<body>



{{--    header--}}

@include('layouts.frontLayout.front_header')

{{--   heder End--}}



{{--    content--}}

@yield('content')

{{--    content End--}}



{{--  Footer--}}

@include('layouts.frontLayout.front_footer')

{{--  Footer End--}}



<script src="{{ asset('js/frontend_js/jquery.js') }}"></script>
<script src="{{ asset('js/frontend_js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/frontend_js/jquery.scrollUp.min.js') }}"></script>
<script src="{{ asset('js/frontend_js/price-range.js') }}"></script>
<script src="{{ asset('js/frontend_js/jquery.prettyPhoto.js') }}"></script>
<script src="{{ asset('js/frontend_js/easyzoom.js') }}"></script>
<script src="{{ asset('js/frontend_js/main.js') }}"></script>
<script src="{{ asset('js/frontend_js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/frontend_js/passtrength.js') }}"></script>


@yield('js')

<script type="text/javascript">
    $(document).ready(function($) {
        $('#myPassword').passtrength({
            minChars: 6,
            passwordToggle: true,
            tooltip: true,
            eyeImg: "/images/frontend_images/eye.svg"
        });
    });
</script>

<!-- Go to www.addthis.com/dashboard to customize your tools -->
{{--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f7e921b70f3835a">--}}
{{--    --}}
{{--</script>--}}


</body>
</html>
