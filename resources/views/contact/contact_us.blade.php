
<?php
use App\Count;

?>
@extends('layouts.frontLayout.front_design')
@section('content')


    <section>
        <div class="container">
            <div class="row">


                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Category</h2>

                        <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                            <div class="panel panel-default">
                                @foreach($categories as $cat)
                                    @if($cat->status=='1')
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordian" href="#{{$cat->id}}">
                                                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>

                                                    <a>{{$cat->name}}</a>

                                                </a>
                                            </h4>
                                        </div>
                                    @endif
                                    <div id="{{$cat->id}}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul>
                                                @foreach($cat->categories as $subcat)
                                                    <?php $CategoryCount = Count::CategoryCount($subcat->id) ; ?>
                                                    @if($subcat->status=='1')
                                                        <li>
                                                            <a href="{{route('listing',$cat->url)}}">{{$subcat->name}} ({{$CategoryCount}})
                                                            </a>
                                                        </li>
                                                    @endif
                                                @endforeach

                                            </ul>
                                        </div>
                                    </div>

                                @endforeach

                            </div>

                        </div>



                        <div class="price-range"><!--price-range-->
                            <h2>Price Range</h2>
                            <div class="well">
                                <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600"
                                       data-slider-step="5" data-slider-value="[250,450]" id="sl2"><br/>
                                <b>$ 0</b> <b class="pull-right">$ 600</b>
                            </div>
                        </div><!--/price-range-->

                        <div class="shipping text-center"><!--shipping-->
                            <img src="{{asset('images/frontend_images/home/shipping.jpg')}}" alt=""/>
                        </div><!--/shipping-->

                    </div>
                </div>



                <div class="col-sm-9 padding-right">
                    <div class="features_items">

                        <h2 class="title text-center">Contact Us</h2>
                        @if(Session::has('error'))
                            <div class="alert alert-error alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('error') !!}</strong>
                            </div>
                        @endif
                            @if(Session::has('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('success') !!}</strong>
                            </div>
                         @endif

                        <div class="contact-form">
                            <div class="status alert alert-success" style="display: none"></div>
                            <form action="{{route('send_contact_info')}}" id="main-contact-form" class="contact-form row" name="main-contact-form" method="post">{{ csrf_field() }}
                                <div class="form-group col-md-6">
                                    <input type="text"  id="name" placeholder="Name"  class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ old('name') }}" >
                                    @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                     <strong>{{ $errors->first('name') }}</strong>
                                     </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="email"  id="email" class="form-control" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') }}" >
                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="text"  id="subject" class="form-control" placeholder="Subject" class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}"
                                           name="subject" value="{{ old('subject') }}" >

                                     @if ($errors->has('subject'))
                                        <span class="invalid-feedback" role="alert">
                                     <strong>{{ $errors->first('subject') }}</strong>
                                     </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-12">
                                    <textarea  id="message" class="form-control {{ $errors->has('message') ? ' is-invalid' : '' }}"
                                               name="message" value="{{ old('message') }}" rows="8" placeholder="Your Message Here" ></textarea>
                                    @if ($errors->has('message'))
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-12">
                                    <input type="submit" name="submit" class="btn btn-primary pull-right" value="Submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection












