
<html>
<body>
<table width='700px'>
    <tr><td>&nbsp;</td></tr>
    <tr><td><img src="{{ asset('images//frontend_imageshome/logo.png') }}"></td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>Hello {{ $name }},</td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>Thank you for shopping with us. Your order details are as below :-</td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>Order No: {{ $order_id }}</td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>
            <table width='95%' cellpadding="5" cellspacing="5" bgcolor="#f7f4f4">
                <tr bgcolor="#cccccc">
                    <td>Product Name</td>
                    <td>Product Code</td>
                    <td>Size</td>
                    <td>Color</td>
                    <td>Quantity</td>
                    <td>Unit Price</td>
                </tr>
                @foreach($productDetails as $product)
                    @foreach($product->order_products as $order)
                    <tr>
                        <td>{{ $order->product_name }}</td>
                        <td>{{ $order->product_code}}</td>
                        <td>{{ $order->product_size}}</td>
                        <td>{{ $order->product_color }}</td>
                        <td>{{ $order->product_qty }}</td>
                        <td>$ {{ $order->product_price }} | (BDT {{$order->product_price*80}})</td>
                    </tr>
                    @endforeach
{{--                <tr>--}}
{{--                    <td colspan="5" align="right">Shipping Charges</td><td>${{ $product->shipping_charges }}</td>--}}
{{--                </tr>--}}
               <tr>
                   <td colspan="5" align="right">Coupon Discount</td><td>$ {{ $product->coupon_amount}} | (BDT {{ $product->coupon_amount*80}})</td>
               </tr>
                        <tr>
                            <td colspan="5" align="right">Shipping Charges</td><td>$ {{$product->shipping_charges}} | (BDT {{$product->shipping_charges*80}})</td>
                        </tr>
                <tr>
                    <td colspan="5" align="right">Grand Total</td><td>$ {{ $product->grand_total }} |  (BDT {{ $product->grand_total*80 }})</td>
                </tr>
            </table>
        </td></tr>
    <tr><td>
            <table width="100%">
                <tr>
                    <td width="50%">
                        <table>
                            <tr>
                                <td><strong>Bill To :-</strong></td>
                            </tr>
                            <tr>
                                <td>{{ $userBillDetails->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ $userBillDetails->address }}</td>
                            </tr>
                            <tr>
                                <td>{{ $userBillDetails->city }}</td>
                            </tr>
                            <tr>
                                <td>{{ $userBillDetails->state }}</td>
                            </tr>
                            <tr>
                                <td>{{ $userBillDetails->country }}</td>
                            </tr>
                            <tr>
                                <td>{{ $userBillDetails->pincode }}</td>
                            </tr>
                            <tr>
                                <td>{{ $userBillDetails->mobile }}</td>
                            </tr>
                        </table>
                    </td>
                    <td width="50%">
                        <table>
                            <tr>
                                <td><strong>Ship To :-</strong></td>
                            </tr>
                            <tr>
                                <td>{{ $product->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ $product->address }}</td>
                            </tr>
                            <tr>
                               <td>{{ $product->city }}</td>
                            </tr>
                            <tr>
                              <td>{{ $product->state }}</td>
                            </tr>
                            <tr>
                               <td>{{ $product->country}}</td>
                            </tr>
                            <tr>
                               <td>{{ $product->pincode }}</td>
                            </tr>
                            <tr>
                               <td>{{ $product->mobile }}</td>
                            </tr>
                        </table>
                        @endforeach
                    </td>
                </tr>
            </table>
        </td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>For any enquiries, you can contact us at <a href="mailto:info@ecom-website.com">info@ecom-website.com</a></td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>Regards,<br> Team E-com</td></tr>
    <tr><td>&nbsp;</td></tr>
</table>
</body>
</html>

