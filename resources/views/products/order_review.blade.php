@extends('layouts.frontLayout.front_design')
@section('content')
    <?php use App\Product; ?>

 <section id="form" style="margin-top:20px;">
  <div class="container">
   <div class="breadcrumbs">
    <ol class="breadcrumb">
     <li><a href="#">Home</a></li>
     <li class="active">Order Review</li>
    </ol>
   </div>


    <div class="row">
     @if(Session::has('error'))
      <div class="alert alert-danger alert-block">
       <button type="button" class="close" data-dismiss="alert">×</button>
       <strong>{!! session('error') !!}</strong>
      </div>
     @endif

     @if(Session::has('success'))
      <div class="alert alert-success alert-block">
       <button type="button" class="close" data-dismiss="alert">×</button>
       <strong>{!! session('success') !!}</strong>
      </div>
     @endif
     {{-- Bill address --}}

     <div class="col-sm-4 col-sm-offset-1">
      <div class="login-form">
       <h2>Billing Details</h2>

       <div class="form-group">
       {{Auth::user()->name}}
       </div>

       <div class="form-group">
        {{Auth::user()->profile->address}}
       </div>

       <div class="form-group">
       {{Auth::user()->profile->city}}
       </div>

       <div class="form-group">
       {{Auth::user()->profile->state}}
       </div>

       <div class="form-group">
       {{Auth::user()->profile->country}}
       </div>

       <div class="form-group">
      {{Auth::user()->profile->state}}
       </div>

       <div class="form-group">
      {{Auth::user()->profile->mobile}}
       </div>
      </div>
     </div>


     <div class="col-sm-1">
      <h2></h2>
     </div>


     {{--ship address--}}

     <div class="col-sm-4">

      <div class="signup-form">
       <h2>Shipping Details</h2>
       <div class="form-group">
      {{Auth::user()->delivery->name}}
       </div>

       <div class="form-group">
        {{Auth::user()->delivery->address}}
       </div>

       <div class="form-group">
       {{Auth::user()->delivery->city}}
       </div>

       <div class="form-group">
       {{Auth::user()->delivery->state}}
       </div>

       <div class="form-group">
      {{Auth::user()->delivery->country}}
       </div>

       <div class="form-group">
       {{Auth::user()->delivery->pincode}}
       </div>

       <div class="form-group">
       {{Auth::user()->delivery->mobile}}
       </div>
      </div>
     </div>
    </div>



   <div class="review-payment">
    <h2>Review & Payment</h2>
   </div>

   <div class="table-responsive cart_info table-bordered">
    <table class="table table-condensed">
     <thead>
     <tr class="cart_menu bg-light" style="background-color: #00AA9E;">
      <td class="image">Item</td>
      <td class="description"></td>
      <td class="price">Price</td>
      <td class="quantity">Quantity</td>
      <td class="total">Total</td>
      <td></td>
     </tr>
     </thead>
     <tbody>
     <?php $total_amount = 0; ?>
     @foreach($userCart as $cart)
      <tr>
       <td class="cart_product">
        <a  href="#"><img style="width: 80px;" src="{{ asset('images/backend_images/products/small/'.$cart->product->image) }}" alt=""></a>
       </td>

       <td class="cart_description">
        <h4><a href="">{{$cart->product_name}}</a></h4>
        <p>CODE:  {{ $cart->product_code }}   |    SIZE: {{ $cart->size }}</p>
       </td>
       <td class="cart_price">
        <p>$ {{$cart->price}} | (BDT {{$cart->price*80}})</p>
       </td>
       <td class="cart_quantity">
        <div class="cart_quantity_button">

         <a class="cart_quantity_up" href="{{ url('/cart/update-quantity/'.$cart->id.'/1') }}"> + </a>
         <input class="cart_quantity_input" type="text" name="quantity" value="{{ $cart->quantity}}" autocomplete="off" size="5" readonly>

         @if($cart->quantity>1)
          <a class="cart_quantity_down" href="{{ url('/cart/update-quantity/'.$cart->id.'/-1') }}"> - </a>
         @endif

        </div>
       </td>

       <td class="cart_total">
        <p class="cart_total_price">$ {{$cart->price*$cart->quantity}} | (BDT {{$cart->price*$cart->quantity*80}})</p>
       </td>
       <td class="cart_delete">
        <a class="cart_quantity_delete" href="{{route('deletecart',$cart->id)}}"><i class="fa fa-times"></i></a>
       </td>
      </tr>
      <?php $total_amount = $total_amount + ($cart->price*$cart->quantity); ?>
     @endforeach
     </tbody>
    </table>

   </div>


  </div>
 </section>



 <section id="do_action">
  <div class="container">
   <div class="row">
    <div class="col-sm-7">


      <div class="total_area">
        <ul>

          @if(!empty(Session::get('CouponAmount')))
            <li>Cart Sub Total <span>$<?php echo $total_amount; ?> | (BDT <?php echo $total_amount*80; ?>) </span></li>
            <li>Shipping Cost <span>${{ $shippingCharges }} | (BDT {{ $shippingCharges*80 }}) </span></li>
            <li style="color:darkblue">Coupon Discount (-) <span>$<?php echo Session::get('CouponAmount'); ?> | (BDT <?php echo Session::get('CouponAmount')*80;?>)
         </span></li>
            <li style="color: red; font-weight: bold;">Grand Total <span>$<?php echo  $total_amount = $total_amount + $shippingCharges - Session::get('CouponAmount'); ?> |  (BDT <?= $total_amount*80 ?>)</span> </li>
          @else
             <li>Shipping Cost <span>$ {{ $shippingCharges }} | (BDT {{ $shippingCharges*80 }}) </span></li>
            <li style="color: red; font-weight: bold;">Total <span >$ <?php echo $total_amount = $total_amount + $shippingCharges; ?> | (BDT <?= $total_amount*80 ?>)</span> </li>
          @endif
        </ul>


      </div>


    </div>




{{--       Order Replacement--}}
    <div class="col-sm-5">
              <form name="paymentForm" id="paymentForm" action="{{route('placeorder')}}" method="post">
                  @csrf
                <div class="chose_area">
                  <ul class="user_option">
                    <input type="hidden" name="grand_total" value="{{ $total_amount }}">
                <span>
         <label><strong>Select Payment Method:</strong></label>
        </span>

                      @if($codpincodeCount>0)
           <span>
         <label>
        <input class="form-check-input" type="radio" name="payment_method" id="COD" value="COD"> Cash On Delivary
          </label>
        </span>
              @endif
             @if($paypalpincodeCount>0)
                <span>
         <label>
        <input class="form-check-input" type="radio" name="payment_method" id="Paypal" value="Paypal"> Paypal
       </label>
        </span>
           @endif


        <button class="btn btn-default check_out" name="submit"  onclick="return selectPaymentMethod();" >Order Replacement</button>

                  </ul>
                </div>
              </form>

    </div>

{{--       Order Replacement--}}





   </div>
  </div>

 </section>

@endsection
