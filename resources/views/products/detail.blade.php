<?php
use App\Count;

?>
@extends('layouts.frontLayout.front_design')
@section('content')


    <section>
        <div class="container">
            <div class="row">


                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Category</h2>

                        <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                            <div class="panel panel-default">
                                @foreach($categories as $cat)
                                    @if($cat->status=='1')
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordian" href="#{{$cat->id}}">
                                                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>

                                                    <a>{{$cat->name}}</a>

                                                </a>
                                            </h4>
                                        </div>
                                    @endif
                                    <div id="{{$cat->id}}" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <ul>
                                                @foreach($cat->categories as $subcat)
                                                    <?php $CategoryCount = Count::CategoryCount($subcat->id); ?>
                                                    @if($subcat->status=='1')
                                                        <li>
                                                            <a href="{{route('listing',$cat->url)}}">{{$subcat->name}}
                                                                ({{$CategoryCount}})
                                                            </a>
                                                        </li>
                                                    @endif
                                                @endforeach

                                            </ul>
                                        </div>
                                    </div>

                                @endforeach

                            </div>

                        </div>


{{--                        <div class="brands_products"><!--brands_products-->--}}
{{--                            <h2>Brands</h2>--}}
{{--                            <div class="brands-name">--}}
{{--                                <ul class="nav nav-pills nav-stacked">--}}
{{--                                    <li><a href=""> <span class="pull-right">(50)</span>Acne</a></li>--}}
{{--                                    <li><a href=""> <span class="pull-right">(56)</span>Grüne Erde</a></li>--}}
{{--                                    <li><a href=""> <span class="pull-right">(27)</span>Albiro</a></li>--}}
{{--                                    <li><a href=""> <span class="pull-right">(32)</span>Ronhill</a></li>--}}
{{--                                    <li><a href=""> <span class="pull-right">(5)</span>Oddmolly</a></li>--}}
{{--                                    <li><a href=""> <span class="pull-right">(9)</span>Boudestijn</a></li>--}}
{{--                                    <li><a href=""> <span class="pull-right">(4)</span>Rösch creative culture</a></li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div><!--/brands_products-->--}}

{{--                        <div class="price-range"><!--price-range-->--}}
{{--                            <h2>Price Range</h2>--}}
{{--                            <div class="well">--}}
{{--                                <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600"--}}
{{--                                       data-slider-step="5" data-slider-value="[250,450]" id="sl2"><br/>--}}
{{--                                <b>$ 0</b> <b class="pull-right">$ 600</b>--}}
{{--                            </div>--}}
{{--                        </div><!--/price-range-->--}}

                        <div class="shipping text-center"><!--shipping-->
                            <img src="{{asset('images/frontend_images/home/shipping.jpg')}}" alt=""/>
                        </div><!--/shipping-->

                    </div>
                </div>


                <div class="col-sm-9 padding-right">
                    <div class="product-details">
                        @if(Session::has('error'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('error') !!}</strong>
                            </div>
                        @endif

                        @if(Session::has('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{!! session('success') !!}</strong>
                            </div>
                        @endif


                        <div class="col-sm-5">

                            {{-- Main Image and zoom --}}
                            <div class="view-product">
                                <div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails">
                                    @if(empty($product->image))
                                        <a href="{{ asset('images/backend_images/products/large//small/fake_avatar/p6.jpg') }}">
                                            <img style="width: 350px;" class="mainImage"
                                                 src="{{ asset('images/backend_images/products/small/fake_avatar/p6.jpg') }}"
                                                 alt=""/>
                                        </a>
                                    @else

                                        <a href="{{ asset('images/backend_images/products/large/'.$product->image) }}">
                                            <img style="width: 350px;" class="mainImage"
                                                 src="{{ asset('images/backend_images/products/small/'.$product->image) }}"
                                                 alt=""/>
                                        </a>
                                    @endif

                                </div>

                            </div>
                            {{--   Main Image End--}}


                            <div id="similar-product" class="carousel slide" data-ride="carousel">

                                <div class="carousel-inner">

                                    <div class="item active thumbnails ">
                                        @foreach($product->images as $altimage)


                                            <a href="{{asset('images/backend_images/products/large/'.$altimage->image)}}"
                                               data-standard="{{asset('images/backend_images/products/small/'.$altimage->image)}}">

                                                <img class="changeImage" style="width: 80px; cursor: pointer;"
                                                     src="{{asset('images/backend_images/products/small/'.$altimage->image)}}"
                                                     alt="">

                                            </a>


                                        @endforeach

                                    </div>


                                </div>

                            </div>

                        </div>


                        {{-- Change Price with Size & change stock utility.using js --}}
                        {{--  Add to cart--}}


                        <div class="col-sm-7">
                            <form name="addtoCartForm" id="addtoCartForm" action="{{route('addtocart')}}" method="post">
                                @csrf

                                <input type="hidden" name="product_id" value="{{ $product->id }}">
                                <input type="hidden" name="product_name" value="{{ $product->product_name }}">
                                <input type="hidden" name="product_code" value="{{ $product->product_code }}">
                                <input type="hidden" name="product_color" value="{{ $product->product_color }}">
                                <input type="hidden" name="price" value="{{ $product->price }}" id="price_add">
                                <input type="hidden" name="size" type="text" value="" id="size_add">
                                <input type="hidden" name="sku" type="text" value="{{ $product->sku}}" id="sku">


                                <div class="product-information">


                                    <h2>{{$product->product_name}}</h2>
                                    <p>code: {{$product->product_code}}</p>


                                    <p>
                                        <select style="width:150px;" id="product_size" required>
                                            <option>Select Size</option>
                                            @foreach($prices as $price)

                                                <option value="{{ $price->size }}">{{ $price->size }}</option>

                                            @endforeach
                                        </select>

                                    </p>

                                    <span>
 <span>



     <span id="attr_price_on_size"><span id="attr_price_on_size_content">${{ $product->price }}</span><br>
         <span style="font-size:15px;" id="hiddeDollarprice">({{ $product->price*80 }} BDT)</span>
{{--         <span style="font-size:15px;" id="dollarbdtshow"><span id="dollarbdt"></span></span>--}}
     </span>
     </span>


 </span>


<div  style="position: relative;bottom: 100px;">
    <div  style="position: relative;left:150px;">
    <label>Quantity:</label>
    <input name="quantity" type="text" value="1"/><br>
    </div>
        <button type="submit" style="float: right;margin-top: 10px;margin-right: 30px;" id="cart_btn" class="btn btn-fefault cart">
    <i class="fa fa-shopping-cart"></i>
    Add to cart
    </button>
</div>

{{--</span>--}}


                                    <p><b>Availability:</b> <span class="product_stock">In Stock</span></p>
                                    <p><b>Condition:</b> New</p>
                                    <p><b>Brand:</b> E-SHOPPER</p>

                                    {{-- Check Pincode--}}
                                    <p><b>Delivery</b>
                                        <input type="text" name="pincode" id="chkPincode" placeholder="Check Pincode">
                                        <button type="button" onclick="return checkPincode();">Go</button>
                                        <span id="pincodeResponse"></span>
                                    <div style="float:left; margin-top: 10px;" class="sharethis-inline-share-buttons"></div>
                                    {{-- Check Pincode End--}}

                                </div>
                            </form>
                        </div>
                    </div>


                    {{-- Change Price with Size & change stock utility.using js end --}}
                    {{--  Add to cart End --}}


                    <div class="category-tab shop-details-tab"><!--category-tab-->
                        <div class="col-sm-12">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#description" data-toggle="tab">Description</a></li>

                                <li><a href="#care" data-toggle="tab">Material & Care</a></li>
                                <li><a href="#delivery" data-toggle="tab">Delivery Options</a></li>
                                @if(!empty($product->video))
                                    <li>
                                        <a href="#video" data-toggle="tab">Product Video</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <div class="tab-content">

                            <div class="tab-pane active" id="description">

                                <div class="col-sm-12">
                                    <p>

                                        {{$product->description}}
                                    </p>

                                </div>

                            </div>


                            <div class="tab-pane fade" id="care">
                                <div class="col-sm-12">
                                    <p>
                                        {{$product->care}}
                                    </p>

                                </div>

                            </div>


                            <div class="tab-pane fade" id="delivery">
                                <div class="col-sm-12">
                                    <p>
                                        Paypal,Country wise(shipping),Cash on Delivary<br>
                                        Made In Japan
                                    </p>

                                </div>

                            </div>


                            @if(!empty($product->video))
                                <div class="tab-pane fade" id="video">
                                    <div class="col-sm-12">
                                        <video controls width="640" height="480">
                                            <source src="{{ url('videos/'.$product->video)}}" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                            @endif


                        </div>
                    </div>


                    <div class="recommended_items">
                        <h2 class="title text-center">recommended items</h2>

                        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">

                            <div class="carousel-inner">
                                <div class="item active">

                                    @foreach($recommended_products as $recommended_product)
                                        <div class="col-sm-4">
                                            <div class="product-image-wrapper">
                                                <div class="single-products">
                                                    <div class="productinfo text-center">

                                                        @if(empty($recommended_product->image))

                                                            <img style="width:150px;"
                                                                 src="{{ asset('images/backend_images/products/small/fake_avatar/p6.jpg')}}">

                                                        @else
                                                            <img style="width:150px;"
                                                                 src="{{ asset('images/backend_images/products/small/'.$recommended_product->image) }}">

                                                        @endif

                                                        <h2>${{$recommended_product->price}}</h2>
                                                        <p>{{$recommended_product->product_name}}</p>
                                                        <a href="{{route('productsdetails',$recommended_product->slug)}}"
                                                           class="btn btn-default add-to-cart"><i
                                                                class="fa fa-shopping-cart"></i>Add to cart</a>
                                                        {{--                                                    <button type="button" class="btn btn-default add-to-cart">--}}
                                                        {{--                                                        <i class="fa fa-shopping-cart"></i>--}}
                                                        {{--                                                        Add to cart</button>--}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>

                            <a class="left recommended-item-control" href="#recommended-item-carousel"
                               data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a class="right recommended-item-control" href="#recommended-item-carousel"
                               data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>



@endsection

@section('js')

    <script type="text/javascript">


        // ************   Display Product Price Based On Prouct_id and size   ******************************

        $('#product_size').on('change', function (e) {

            var product_size = e.target.value;
            var product_id = {{ $product->id }};


            console.log(product_size);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({

                url: "{{ route('price_display_on_size') }}",
                type: "POST",
                data: {
                    product_id: product_id,
                    product_size: product_size


                },

                success: function (data) {

                    $('#price_add').attr('value', data.product_att_price.price);
                    $('#size_add').attr('value', product_size);
                    // Below two line is used for testing
                    console.log(data.product_att_price.price);
                    console.log(data.product_att_price.stock);
                    // Adding Price and Size

                    // Checking empty or not
                    var dollarprice=data.product_att_price.price;
                    var dollarsize='$'
                    var dolres=dollarsize+''+dollarprice;
                    var dollarratebdt='('+dollarprice*80+' BDT)';

                    $("#attr_price_on_size_content").html(dolres);
                    $("#hiddeDollarprice").html(dollarratebdt);
                    // $("#dollarbdtshow").show();
                    // $("#dollarbdt").html(dollarratebdt);

                    if (data.product_att_price.stock > 0) {
                        $('.product_stock').text("In Stock");
                        $("#cart_btn").removeClass('disabled');

                    } else {
                        $('.product_stock').text("Sold Out");
                        $("#cart_btn").addClass('disabled');

                    }
                }
            });


        });


    </script>


@endsection





