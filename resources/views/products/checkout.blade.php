
@extends('layouts.frontLayout.front_design')
@section('content')

  <section id="form" style="margin-top:20px;">
    <div class="container">
      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">Check Out</li>
        </ol>
      </div>
      @if(Session::has('error'))
         <div class="alert alert-danger alert-block">
         <button type="button" class="close" data-dismiss="alert">×</button>
         <strong>{!! session('error') !!}</strong>
         </div>
       @endif

      @if(Session::has('success'))
        <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{!! session('success') !!}</strong>
        </div>
       @endif


      <form action="{{route('checkoutproduct')}}" method="post">{{ csrf_field() }}
        <div class="row">
          {{-- Bill address --}}

          <div class="col-sm-4 col-sm-offset-1">
            <div class="login-form">
              <h2>Bill To</h2>

              <div class="form-group">
                <input name="billing_name" id="billing_name" @if(!empty(Auth::user()->name)) value="{{Auth::user()->name}}" @endif type="text" placeholder="Billing Name" class="form-control" />
              </div>

              <div class="form-group">
                <input name="billing_address" id="billing_address" @if(!empty(Auth::user()->profile->address)) value="{{Auth::user()->profile->address}}"  @endif type="text" placeholder="Billing Address" class="form-control" />
              </div>

              <div class="form-group">
                <input name="billing_city" id="billing_city" @if(!empty(Auth::user()->profile->city)) value="{{Auth::user()->profile->city}}" @endif type="text" placeholder="Billing City" class="form-control" />
              </div>

              <div class="form-group">
                <input name="billing_state" id="billing_state" @if(!empty(Auth::user()->profile->city)) value="{{Auth::user()->profile->state}}" @endif type="text" placeholder="Billing State" class="form-control" />
              </div>

              <div class="form-group">
                <select id="billing_country" name="billing_country" class="form-control" >
                  <option value="">Select Country</option>
{{--                  @foreach(App\Profile::all() as $country)--}}
{{--                <option value="{{$country->country}}" @if($country->country == Auth::user()->profile->country) selected @endif>--}}
{{--                {{$country->country}}--}}
{{--                  </option>--}}
{{--                  @endforeach--}}


                    @foreach(App\Country::all() as $country)
                        <option  value="{{ $country->country_name }}"
                                 @if( Auth::user()->profile->country== $country->country_name) selected @endif>
                            {{ $country->country_name }}
                        </option>
                    @endforeach

                </select>
              </div>

              <div class="form-group">
                <input name="billing_pincode" id="billing_pincode" @if(!empty(Auth::user()->profile->pincode)) value="{{Auth::user()->profile->pincode}}" @endif type="text" placeholder="Billing Pincode" class="form-control" />
              </div>

              <div class="form-group">
                <input name="billing_mobile" id="billing_mobile" @if(!empty(Auth::user()->profile->mobile)) value="{{Auth::user()->profile->mobile}}" @endif type="text" placeholder="Billing Mobile" class="form-control" />
              </div>

              <div class="form-check">
                <input type="checkbox" class="form-check-input" id="copyAddress">
                <label class="form-check-label" for="copyAddress">Shipping Address same as Billing Address</label>
              </div>

            </div>
          </div>


          <div class="col-sm-1">
            <h2></h2>
          </div>


          {{--ship address--}}

          <div class="col-sm-4">

            <div class="signup-form">
              <h2>Ship To</h2>
              <div class="form-group">
                <input  @if(!empty(Auth::user()->delivery->name)) value="{{Auth::user()->delivery->name}}" @endif name="shipping_name" id="shipping_name"    type="text" placeholder="Shipping Name" class="form-control" required/>
              </div>

              <div class="form-group">
                <input  @if(!empty(Auth::user()->delivery->address)) value="{{Auth::user()->delivery->address}}" @endif name="shipping_address" id="shipping_address" type="text" placeholder="Shipping Address" class="form-control" required/>
              </div>

              <div class="form-group">
                <input  @if(!empty(Auth::user()->delivery->city)) value="{{Auth::user()->delivery->city}}" @endif name="shipping_city" id="shipping_city"  type="text" placeholder="Shipping City" class="form-control" required/>
              </div>

              <div class="form-group">
                <input  @if(!empty(Auth::user()->delivery->state)) value="{{Auth::user()->delivery->state}}" @endif name="shipping_state" id="shipping_state"  type="text" placeholder="Shipping State" class="form-control" required/>
              </div>

              <div class="form-group">
              <select id="shipping_country" name="shipping_country" class="form-control" required>

                  <option value="">Select Country</option>
{{--                @foreach(App\Profile::all() as $country)--}}
{{--                  <option value="{{$country->country}}" @if($country->country == Auth::user()->profile->country) selected @endif>--}}
{{--                    {{$country->country}}--}}
{{--                  </option>--}}
{{--                @endforeach--}}


{{--                  @foreach(App\Country::all() as $country)--}}
{{--                      <option  value="{{ $country->country_name }}"--}}
{{--                               @if( Auth::user()->profile->country== $country->country_name) selected @endif>--}}
{{--                          {{ $country->country_name }}--}}
{{--                      </option>--}}
{{--                  @endforeach--}}

                  @foreach(App\Country::all() as $country)
                      <option  value="{{ $country->country_name }}">
                          {{ $country->country_name }}
                      </option>
                  @endforeach

                </select>
              </div>

              <div class="form-group">
                <input @if(!empty(Auth::user()->delivery->pincode)) value="{{Auth::user()->delivery->pincode}}" @endif  name="shipping_pincode" id="shipping_pincode" type="text" placeholder="Shipping Pincode" class="form-control" required/>
              </div>

              <div class="form-group">
                <input  @if(!empty(Auth::user()->delivery->mobile)) value="{{Auth::user()->delivery->mobile}}" @endif   name="shipping_mobile" id="shipping_mobile"  type="text" placeholder="Shipping Mobile" class="form-control" required/>
              </div>
              <button href="{{route('order_review')}}" type="submit" class="btn btn-default">Check Out</button>
            </div>

          </div>


        </div>
      </form>
    </div>
  </section>

@endsection
