
@extends('layouts.frontLayout.front_design')
@section('content')
  <section id="form">
    <div class="container">
      <div class="row">
          @if(Session::has('error'))
              <div class="alert alert-danger alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{!! session('error') !!}</strong>
              </div>
          @endif

        @if(Session::has('success'))


          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{!! session('success') !!}</strong>
          </div>
        @endif


        <div class="col-md-4 col-md-offset-1">
          <div class="login-form">
            <h2>Update Your Account Information</h2>
            <form action="{{route('profile')}}" method="post">
              @csrf
              <div class="form-group row">
                <div class="col-md-12">
                  <input value="{{Auth::user()->email}}" readonly>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-12">
                  <input value="{{Auth::user()->id}}"  readonly>
                </div>
              </div>

              <div class="form-group row">
            <div class="col-md-12">
             <textarea name="address" class="form-control" rows="3" placeholder="Address">{{Auth::user()->profile->address}}</textarea>
              {{--   {{Auth::user()->profile->address}} --}}
             @if ($errors->has('address'))
              <span class="invalid-feedback" role="alert">
         <strong>{{ $errors->first('address') }}</strong>
        </span>
             @endif
            </div>
           </div>


              <div class="form-group row ">
                <div class="col-md-12">
                  <input value="{{Auth::user()->profile->city}}"   id="city" placeholder="City" type="text"  name="city" >
                  {{--      value="{{Auth::user()->profile->city}}"  --}}

                </div>
              </div>


              <div class="form-group row ">
                <div class="col-md-12">
                  <input value="{{Auth::user()->profile->state}}"  id="state" placeholder="State" type="text" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" name="state"  >
             {{--  value="{{Auth::user()->profile->state}}" --}}

                </div>
              </div>





              <div class="form-group row ">
                <div class="col-md-12">
                  <input  value="{{Auth::user()->profile->pincode}}" id="pincode" placeholder="Pincode" type="text" class="form-control{{ $errors->has('pincode') ? ' is-invalid' : '' }}" name="pincode" >
                {{--    value="{{Auth::user()->profile->pincode}}" --}}

                </div>
              </div>


              <div class="form-group row ">
                <div class="col-md-12">

                    <select id="country" name="country">
                        <option value="">Select Country</option>
                        @foreach(App\Country::all() as $country)
                            <option  value="{{ $country->country_name }}"
                                     @if( Auth::user()->profile->country== $country->country_name) selected @endif>
                                {{ $country->country_name }}
                            </option>
                        @endforeach
                    </select>

                </div>
              </div>






              <div class="form-group row ">
                <div class="col-md-12">
                  <input value="{{Auth::user()->profile->mobile}}"  id="mobile" placeholder="mobile" type="text" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" >

                </div>
              </div>

         <button type="submit" class="btn btn-default">Update</button>
            </form>
          </div>
        </div>



        <div class="col-md-1">
          <h2 class="or">OR</h2>
        </div>


            <div class="col-md-6">
                <div class="signup-form">
                    <h2>Update Password!</h2>
                    <form action="{{route('uppwd') }}" method="post" name="password_validate" id="password_validate" novalidate="novalidate">
                        @csrf

                        <div class="form-group row ">
                            <div class="col-md-8">
                                <input type="password" name="current_pwd" id="current_pwd" placeholder="Current Password" />
                                <span id="chkPwd"></span>
                            </div>
                        </div>

                        <div class="form-group row ">
                            <div class="col-md-8">
                                <input type="password" name="new_pwd" id="new_pwd" placeholder="New Password"/>
                            </div>
                        </div>

                        <div class="form-group row ">
                            <div class="col-md-8">
                                <input type="password" name="confirm_pwd" id="confirm_pwd" placeholder="Confirm Password"/>
                            </div>
                        </div>

                        <div class="form-group col-md-7 row mb-0">
                            <div class="col-md-7 offset-md-4">
                                <button  type="submit" value="Update Password" class="btn btn-default">Update Password</button>
                            </div>
                        </div>
                    </form>


                </div>
            </div>




      </div>
    </div>
  </section>
@endsection
