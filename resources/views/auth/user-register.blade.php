
@extends('layouts.frontLayout.front_design')
@section('content')
<section id="form">
  <div class="container">
    <div class="row">
      @if(Session::has('error'))
        <div class="alert alert-danger alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{!! session('error') !!}</strong>
        </div>
      @endif

          @if(Session::has('success'))
              <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{!! session('success') !!}</strong>
              </div>
          @endif

      <div class="col-md-4 col-md-offset-1">
        <div class="login-form"><!--login form-->
          <h2>Login to your account</h2>
          <form action="{{url('/')}}" method="post" id="loginform">
            @csrf
            <div class="form-group row">
              <div class="col-md-12">
                <input id="email" placeholder="Email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
              </div>
            </div>

            <div class="form-group row">
              <div class="col-md-12">
                <input id="password" placeholder="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                @if ($errors->has('password'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
              </div>
            </div>
            <span>
    <input type="checkbox" class="checkbox">
    Keep me signed in
   </span>
            <button type="submit" class="btn btn-default">Login</button>
              <a href="{{ url('user/password/forgot-password') }}">Forgot Password?</a>
          </form>
        </div>
      </div>



      <div class="col-md-1">
        <h2 class="or">OR</h2>
      </div>


      <div class="col-md-6">
        <div class="signup-form"><!--sign up form-->
          <h2>New User Signup!</h2>
          <form action="{{route('registration')}}" method="post">
            @csrf
            <div class="form-group row ">
             <div class="col-md-8">
                <input placeholder="Name" id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required>

                @if ($errors->has('name'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                  </span>
                @endif
              </div>
           </div>
            <input type="hidden" value="seeker" name="user_type">


            <div class="form-group row ">
             <div class="col-md-8">
                <input id="email" placeholder="Email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
                @endif
             </div>
            </div>

            <div class="form-group row ">


              <div class="col-md-8">
                <input id="myPassword" placeholder="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                @if ($errors->has('password'))
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
              </div>
           </div>

            <div class="form-group row ">
              <div class="col-md-8">
                <input  placeholder="Confirm Password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
              </div>
            </div>

            <div class="form-group col-md-7 row mb-0">
              <div class="col-md-7 offset-md-4">
                <button type="submit" class="btn btn-default">Register</button>
             </div>
            </div>
          </form>




        </div>
      </div>



    </div>
  </div>
</section>
@endsection
