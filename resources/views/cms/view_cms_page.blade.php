@extends('layouts.adminLayout.admin_design')
@section('content')


<div id="content">
<div id="content-header">
<div id="breadcrumb"><a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>
    Home</a> <a href="#">CMS</a> <a href="#" class="current">View CMS</a></div>
<h1>View CMS</h1>
</div>


@if(Session::has('error'))
<div class="alert alert-error alert-block">
<button type="button" class="close" data-dismiss="alert">×</button>
<strong>{!! session('error') !!}</strong>
</div>
@endif
@if(Session::has('success'))
<div class="alert alert-success alert-block">
<button type="button" class="close" data-dismiss="alert">×</button>
<strong>{!! session('success') !!}</strong>
</div>
@endif


<div class="container-fluid">
<hr>
<div class="row-fluid">
<div class="span12">

    <div class="widget-box">
        <div class="widget-title"><span class="icon"><i class="icon-th"></i></span>
            <h5>View CMS</h5>
        </div>
        <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
                <thead>
                <tr>
                    <th>CMS ID</th>
                    <th>Title</th>
                    <th>Url</th>
                    <th>Description</th>
                    <th>Status</th>
                    <th>Created At</th>
                    <th>Action</th>
                </tr>
                </thead>

                <tbody>
                @foreach($cms_pages as $cms)

                    <tr class="gradeX">
                        <td>{{$cms->id}}</td>
                        <td>{{str_limit($cms->title,15)}}</td>
                        <td>{{str_limit($cms->url,10)}}</td>
                        <td>{{ str_limit($cms->description,25)}}</td>
                        <td>
                            @if($cms->status===1)
                                <h6 class="text-success font-weight-bold">1</h6>

                            @else
                                <h6 class="text-warning font-weight-bold">0</h6>
                            @endif

                        </td>
                        <td>{{$cms->created_at}}</td>


                        <td class="center">
                            <a href="#exampleModal{{$cms->id}}" data-toggle="modal"
                               data-target="#exampleModal{{$cms->id}}" class="btn btn-success btn-mini"
                               title="View cms">View</a>
                            <a href="{{route('edit_cms',$cms->id)}}" class="btn btn-primary btn-mini ">Edit</a>
                            &nbsp;
                            <a href="{{route('delete_cms',$cms->id)}}"
                               onclick="return confirm('Are you sure you want to delete this item?');"
                               class="btn btn-danger btn-mini ">Delete</a> &nbsp;

                        </td>

                    </tr>


                    <div class="modal fade" id="exampleModal{{$cms->id}}" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{$cms->title}}</h5>
                                    {{--                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                                    {{--                                                                            <span aria-hidden="true">&times;</span>--}}
                                    {{--                                                                        </button>--}}
                                </div>
                                <div class="modal-body">

                                    <p>cms ID : <b>{{$cms->id}} </b></p>
                                    <p>Meta Title : <b>{{ str_limit($cms->meta_title, 40)}}</b></p>
                                    <p>Meta Description :
                                        <b>{{ str_limit($cms->meta_description, 40)}} </b></p>
                                    <p>Meta Keywords :<b>$ {{$cms->meta_keywords}}</b></p>

                                    Description : {!! $cms->body_html !!}

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger btn-mini"
                                            data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach
                </tbody>

            </table>
        </div>
    </div>
</div>
</div>
</div>
</div>


@endsection
