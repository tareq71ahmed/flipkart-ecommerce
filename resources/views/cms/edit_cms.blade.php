
@extends('layouts.adminLayout.admin_design')
@section('content')


    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"><a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i>
                    Home</a> <a href="#">CMS</a> <a href="#" class="current">Update CMS</a></div>
            <h1>Update-CMS</h1>
        </div>

        @if(Session::has('error'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! session('error') !!}</strong>
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! session('success') !!}</strong>
            </div>
        @endif

        <div class="container-fluid">
            <hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"><span class="icon"> <i class="icon-info-sign"></i> </span>
                            <h5>Update-CMS</h5>

                        </div>
                        <div class="widget-content nopadding">


                            <form class="form-horizontal" method="post"  name="add_cms"
                                  id="add_cms"  action="{{route('update_cms',$cms->id)}}" novalidate="novalidate">
                                @csrf


                                <div class="control-group">
                                    <label class="control-label">Title</label>
                                    <div class="controls">
                                        <input type="text" id="title"
                                               class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                               name="title" value="{{$cms->title}}"  required>
                                        @if ($errors->has('title'))
                                            <span class="invalid-feedback" role="alert">
                         <strong>{{ $errors->first('title') }}</strong>
                          </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="control-group">
                                    <label class="control-label">URL</label>
                                    <div class="controls">
                                        <input type="text" id="url"
                                               class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}"
                                               name="url" value="{{$cms->url}}" required>

                                        @if ($errors->has('url'))
                                            <span class="invalid-feedback" role="alert">
                         <strong>{{ $errors->first('url') }}</strong>
                          </span>
                                        @endif

                                    </div>
                                </div>


                                <div class="control-group">
                                    <label class="control-label">CMS Description</label>
                                    <div class="controls">
                    <textarea
                        class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                        name="description" value="{{ old('description') }}" required>{{$cms->description}}</textarea>
                                        @if ($errors->has('description'))
                                            <span class="invalid-feedback" role="alert">
                         <strong>{{ $errors->first('description') }}</strong>
                          </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">Meta Title</label>
                                    <div class="controls">
                                        <input type="text" id="meta_title" class="form-control" name="meta_title" value="{{$cms->meta_title}}" >

                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Meta Description</label>
                                    <div class="controls">
                                        <input type="text" id="meta_description" class="form-control" name="meta_description" value="{{$cms->meta_description}}" >

                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Meta Keywords</label>
                                    <div class="controls">
                                        <input type="text" id="meta_keywords" class="form-control" name="meta_keywords" value="{{$cms->meta_keywords}}" >

                                    </div>
                                </div>


                                <div class="control-group">
                                    <label class="control-label">Enable</label>
                                    <div class="controls">
                                        <input type="checkbox" name="status" id="status" @if($cms_status->status == "1") checked @endif value="1">
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <input type="submit" value="Update" class="btn btn-success">
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection
