
 <?php
 use App\Count;

 ?>
@extends('layouts.frontLayout.front_design')
@section('content')


    <section>
        <div class="container">
            <div class="row">


                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Category</h2>

                <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                    <div class="panel panel-default">
                        @foreach($categories as $cat)
                            @if($cat->status=='1')
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordian" href="#{{$cat->id}}">
                                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>

                                    <a>{{$cat->name}}</a>

                                </a>
                            </h4>
                        </div>
                            @endif
                        <div id="{{$cat->id}}" class="panel-collapse collapse">
                            <div class="panel-body">
                                <ul>
                                    @foreach($cat->categories as $subcat)
                                        <?php $CategoryCount = Count::CategoryCount($subcat->id) ; ?>
                                        @if($subcat->status=='1')
                                            <li>
                                                <a href="{{route('listing',$cat->url)}}">{{$subcat->name}} ({{$CategoryCount}})
                                                </a>
                                            </li>
                                        @endif
                                    @endforeach

                                </ul>
                            </div>
                        </div>

                        @endforeach

                    </div>

                </div>



                <div class="price-range"><!--price-range-->
                    <h2>Price Range</h2>
                    <div class="well">
                        <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600"
                               data-slider-step="5" data-slider-value="[250,450]" id="sl2"><br/>
                        <b>$ 0</b> <b class="pull-right">$ 600</b>
                    </div>
                </div><!--/price-range-->

                <div class="shipping text-center"><!--shipping-->
                    <img src="{{asset('images/frontend_images/home/shipping.jpg')}}" alt=""/>
                </div><!--/shipping-->

            </div>
        </div>



            <div class="col-sm-9 padding-right">
                <div class="features_items">
                    <h2 class="title text-center">
                        {{ $cmsPageDetails->title }}
                    </h2>
                    <p>
                        {{ $cmsPageDetails->description }}
                    </p>
                </div>
            </div>
            </div>
        </div>
    </section>



@endsection












