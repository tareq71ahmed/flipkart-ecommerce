


@extends('layouts.adminLayout.admin_design')
{{--@section('title') Admin Category @endsection--}}
@section('content')


    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Add Admin | Sub-Admin</a> <a href="#" class="current">Add Admin | Sub-Admin</a> </div>
            <h1>Add Admin | Sub-Admin</h1>
        </div>

        @if(Session::has('error'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! session('error') !!}</strong>
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! session('success') !!}</strong>
            </div>
        @endif

        <div class="container-fluid"><hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                            <h5>Add Admin | Sub-Admin</h5>

                        </div>
                        <div class="widget-content nopadding">

                            <form class="form-horizontal" method="post" action="{{route('add_Admin_subadmin')}}">
                                @csrf




                                <div class="control-group">
                                    <label class="control-label">Type</label>
                                    <div class="controls">
                                        <select name="user_type" id="user_type" class="control-label"
                                                style="width: 220px;"  >
                                        <option  value="0" >Select Admin Type</option>
                                        <option value="admin">Admin</option>
                                        <option value="sub admin">Sub Admin</option>
                                        </select>
                                    </div>
                                </div>

{{--                                <option value="New" @if($ordersDetails->order_status == "New") selected @endif>New</option>--}}


                                <div class="control-group">
                                    <label class="control-label">Name</label>
                                    <div class="controls">
                                        <input type="text" name="name" id="name">
                                    </div>
                                </div>


                            <div class="control-group">
                                <label class="control-label">Email</label>
                                <div class="controls">
                                    <input type="email" name="email" id="email">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">password</label>
                                <div class="controls">
                                    <input type="password" name="password" id="password">
                                </div>
                            </div>




                                <div class="control-group">
                                    <label class="control-label">Access</label>
                                    <div class="controls">
                                        <input type="checkbox" name="categories_access" id="categories_access" value="1" style="margin-top: -3px;">&nbsp; Categories&nbsp;
                                        <input type="checkbox" name="products_access" id="products_access" value="1" style="margin-top: -3px;">&nbsp; Products&nbsp;
                                        <input type="checkbox" name="orders_access" id="orders_access" value="1" style="margin-top: -3px;"> &nbsp; Orders&nbsp;
                                        <input type="checkbox" name="users_access" id="users_access" value="1" style="margin-top: -3px;">&nbsp; Users&nbsp;
                                    </div>
                                </div>





                                <div class="control-group">
                                    <label class="control-label">Enable</label>
                                    <div class="controls">
                                        <input type="checkbox" name="status" id="status" value="1">
                                    </div>
                                </div>


                                {{-- jquery.uniform.js->654 line,diseble --}}

                                <div class="form-actions">
                                    <input type="submit" value="submit" class="btn btn-success">
                                </div>


                            </form>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>






@endsection

