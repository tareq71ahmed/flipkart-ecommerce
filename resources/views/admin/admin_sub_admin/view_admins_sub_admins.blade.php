@extends('layouts.adminLayout.admin_design')
@section('content')

    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Admin</a> <a href="#" class="current">View All Admin | Sub-Admin</a> </div>
            <h1>View All Admin | Sub-Admin</h1>
        </div>


        @if(Session::has('error'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! session('error') !!}</strong>
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! session('success') !!}</strong>
            </div>
        @endif


        <div class="container-fluid">
            <hr>
            <div class="row-fluid">
                <div class="span12">

                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>View All Admin | Sub-Admin</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>User type</th>
                                    <th>Roles</th>
                                    <th>email</th>
                                    <th>status</th>
                                    <th>Created On</th>
                                    <th>Updated On</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($admins as $admin)

                                    <?php
                                        if($admin->user_type=='admin')
                                            {
                                                $roles ="All";
                                            } else
                                                {
                                                    $roles ="";
                                                    if($admin->categories_access==1)
                                                        {
                                                            $roles .="Categories "  ;
                                                        }

                                                    if($admin->products_access==1)
                                                    {
                                                        $roles .="Products ";
                                                    }

                                                    if($admin->orders_access==1)
                                                    {
                                                        $roles .="Orders ";
                                                    }

                                                    if($admin->users_access==1)
                                                    {
                                                        $roles .="Users ";
                                                    }

                                                }
                                    ?>



                                    <tr class="gradeX">
                                        <td>{{$admin->id}}</td>
                                        <td>{{$admin->user_type}}</td>

                                       <td> {{$roles}}</td>

                                        <td>
                                            {{$admin->email}}
                                        </td>

                                        <td>
                                            @if($admin->status===1)
                                                <h6 class="text-success font-weight-bold">Active</h6>

                                            @else
                                                <h6 class="text-warning font-weight-bold">In Active</h6>
                                            @endif

                                        </td>

                                        <td>
                                            {{$admin->created_at}}

                                        </td>

                                        <td>
                                            {{$admin->updated_at}}

                                        </td>

                                        <td>
                                            <a href="{{route('editAdmin_sub',$admin->id)}}" class="btn btn-mini btn-success">Edit</a> &nbsp;


                                            <a href="#"  class="btn btn-mini btn-danger">Delete</a>

                                        </td>

{{--                                        onclick="return confirm('Are you sure you want to delete this item?');"--}}

                                        {{--                                        <td class="center">--}}
{{--                                            <a href="#exampleModal{{$admin->id}}" data-toggle="modal" data-target="#exampleModal{{$admin->id}}" class="btn btn-success btn-mini" title="View Product">View</a>--}}
{{--                                        </td>--}}

                                    </tr>


{{--                                    <div class="modal fade" id="exampleModal{{$admin->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
{{--                                        <div class="modal-dialog" role="document">--}}
{{--                                            <div class="modal-content">--}}
{{--                                                <div class="modal-header">--}}
{{--                                                    <h5 class="modal-title" id="exampleModalLabel">{{$admin->id}}</h5>--}}
{{--                                                                                                                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                                                                                                                                                                    <span aria-hidden="true">&times;</span>--}}
{{--                                                                                                                                                                                </button>--}}
{{--                                                </div>--}}
{{--                                                <div class="modal-body">--}}

{{--                                                    <p>User ID : <b>{{$admin->id}} </b></p>--}}
{{--                                                    <p>city : <b>{{$admin->profile->city}}</b></p>--}}
{{--                                                    <p>state: <b>{{$admin->profile->state}}</b></p>--}}
{{--                                                    <p>Country: <b> {{$admin->profile->country}}</b></p>--}}
{{--                                                    <p>Current_sign_in_at: <b>{{\Carbon\Carbon::parse($admin->current_sign_in_at)->diffForHumans()}}</b></p>--}}
{{--                                                    <p>Last_sign_in_at: <b>{{\Carbon\Carbon::parse($admin->last_sign_in_at)->diffForHumans()}}</b></p>--}}
{{--                                                    <p>User last url click: <b>{{$admin->user_click}}</b></p>--}}
{{--                                                    <p>User last click time: <b>{{\Carbon\Carbon::parse($admin->user_click_time)->diffForHumans()}}</b></p>--}}

{{--                                                </div>--}}
{{--                                                <div class="modal-footer">--}}
{{--                                                    <button type="button" class="btn btn-danger btn-mini" data-dismiss="modal">Close</button>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
