




@extends('layouts.adminLayout.admin_design')
{{--@section('title') Admin Category @endsection--}}
@section('content')


    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">Update Admin | Sub-Admin</a> <a href="#" class="current">Update Admin | Sub-Admin</a> </div>
            <h1>Update Admin | Sub-Admin</h1>
        </div>

        @if(Session::has('error'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! session('error') !!}</strong>
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! session('success') !!}</strong>
            </div>
        @endif

        <div class="container-fluid"><hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                            <h5>Update  Admin | Sub-Admin</h5>

                        </div>
                        <div class="widget-content nopadding">

                            <form class="form-horizontal" method="post" action="{{route('updateAdmin_sub',$admin->id)}}">
                                @csrf




                                <div class="control-group">
                                    <label class="control-label">Type</label>
                                    <div class="controls">
                                        <input type="text" name="user_type" id="user_type" readonly value="{{$admin->user_type}}">
                                    </div>
                                </div>




                                <div class="control-group">
                                    <label class="control-label">Name</label>
                                    <div class="controls">
                                        <input type="text" name="name" id="name" value="{{$admin->name}}">
                                    </div>
                                </div>


                                <div class="control-group">
                                    <label class="control-label">Email</label>
                                    <div class="controls">
                                        <input type="email" name="email" id="email" value="{{$admin->email}}">
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label class="control-label">password</label>
                                    <div class="controls">
                                        <input type="password" name="password" id="password" >
                                    </div>
                                </div>



                                @if($admin->user_type=='sub admin')

                                <div class="control-group">
                                    <label class="control-label">Access</label>
                                    <div class="controls">
                                        <input type="checkbox" name="categories_access" id="categories_access" value="1" style="margin-top: -3px;" @if ($admin_status->categories_access =='1')  checked @endif>&nbsp; Categories&nbsp;
                                        <input type="checkbox" name="products_access" id="products_access" value="1" style="margin-top: -3px;" @if ($admin_status->products_access =='1')  checked @endif >&nbsp; Products&nbsp;
                                        <input type="checkbox" name="orders_access" id="orders_access" value="1" style="margin-top: -3px;" @if ($admin_status->orders_access =='1')  checked @endif> &nbsp; Orders&nbsp;
                                        <input type="checkbox" name="users_access" id="users_access" value="1" style="margin-top: -3px;" @if ($admin_status->users_access =='1')  checked @endif>&nbsp; Users&nbsp;
                                    </div>
                                </div>
                                @endif




                                <div class="control-group">
                                    <label class="control-label">Enable</label>
                                    <div class="controls">
                                        <input type="checkbox" name="status" id="status" value="1" @if ($admin_status->status =='1')  checked @endif>
                                    </div>
                                </div>


                                {{-- jquery.uniform.js->654 line,diseble --}}

                                <div class="form-actions">
                                    <input type="submit" value="submit" class="btn btn-success">
                                </div>


                            </form>


                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>






@endsection

