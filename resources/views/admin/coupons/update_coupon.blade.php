@extends('layouts.adminLayout.admin_design')
@section('content')

  <div id="content">

    <div class="container-fluid">
      <hr>
      <div class="row-fluid">
        <div class="span12">
          <div class="widget-box">


            @if(Session::has('error'))
              <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! session('error') !!}</strong>
              </div>
            @endif
            @if(Session::has('success'))
              <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! session('success') !!}</strong>
              </div>
            @endif


            <div class="widget-title"><span class="icon"> <i class="icon-info-sign"></i> </span>

              <h5>Update Coupons</h5>

            </div>
            <div class="widget-content nopadding">


              <form class="form-horizontal" action="{{route('update_coupon',$coupon->id)}}" method="post"  name="add_coupon" id="add_coupon">
                @csrf

                <div class="control-group">
                  <label class="control-label">Coupon Code</label>
                  <div class="controls">
                    <input value="{{$coupon->coupon_code}}" type="text" name="coupon_code" id="coupon_code" minlength="5" maxlength="15" required>
                  </div>
                </div>


                <div class="control-group">
                  <label class="control-label">Amount</label>
                  <div class="controls">
                    <input value="{{$coupon->amount}}" type="number" name="amount" id="amount" min="0"  required>
                  </div>
                </div>


                <div class="control-group">
                  <label class="control-label">Amount Type</label>
                  <div class="controls">
                    <select name="amount_type" id="amount_type" style="width: 220px;">

                      <option @if($coupon->amount_type=="Percentage") selected
                          @endif value="Percentage">Percentage
                      </option>
                      <option @if($coupon->amount_type=="Fixed") selected
                          @endif value="Fixed">Fixed
                      </option>


                    </select>
                  </div>
                </div>


                <div class="control-group">
                  <label class="control-label">Expiry Date</label>
                  <div class="controls">
                    <input value="{{$coupon->expiry_date}}" type="text" name="expiry_date" id="expiry_date" autocomplete="off" required>
                  </div>
                </div>


                <div class="control-group">
                  <label class="control-label">Enable</label>
                  <div class="controls">
                    <input type="checkbox" name="status" id="status" value="1"  @if($coupon->status=="1") checked @endif>
                  </div>
                </div>


                <div class="form-actions">
                  <input type="submit" value="Add Coupon" class="btn btn-success">
                </div>
              </form>


            </div>
          </div>
        </div>
      </div>


    </div>
  </div>

@endsection
