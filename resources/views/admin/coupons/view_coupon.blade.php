@extends('layouts.adminLayout.admin_design')
@section('content')

  <div id="content">
    <div id="content-header">

      <div id="breadcrumb"><a href="{{route('admin.dashboard')}}" title="Go to Home" class="tip-bottom"><i
            class="icon-home"></i> Home</a> <a href="#">Coupons</a> <a href="{{route('coupon')}}">Add
          Coupon</a>
        <a href="{{route('view_coupon')}}" class="current">view
          Coupon</a>
      </div>
      <h1>Coupons</h1>

    </div>
    <div class="container-fluid">
      <hr>
      <div class="row-fluid">
        <div class="span12">
            @if(Session::has('error'))
                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('error') !!}</strong>
                </div>
            @endif

            @if(Session::has('success'))
                <div class="alert alert-success alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{!! session('success') !!}</strong>
                </div>
            @endif

          <div class="widget-box">


            <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
              <h5>View Coupon</h5>
            </div>
            <div class="widget-content nopadding">
              <table class="table table-bordered data-table">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Coupon Code</th>
                  <th>Amount</th>
                  <th>Amount Type</th>
                  <th>Expiry Date</th>
                  <th>Status</th>
                  <th>Crated Date</th>
                  <th>Action</th>
                </tr>
                </thead>

                <tbody>

                @foreach($coupons as $coupon)

                  <tr class="gradeX">
                    <td>{{$coupon->id}}</td>
                    <td>{{$coupon->coupon_code}}</td>
                    <td>{{$coupon->amount}}</td>
                    <td>
                      {{$coupon->amount_type}}
                        @if($coupon->amount_type =="Percentage") %
                        @else $
                        @endif
                    </td>
                    <td>{{$coupon->expiry_date}}</td>
                    <td>{{$coupon->status}}</td>
                    <td>{{date_format($coupon->created_at,'F d')}}</td>
                    <td class="center">

                      <a  href="{{route('edit_coupon',$coupon->id)}}" class="btn btn-mini btn-success">Edit</a> &nbsp;
                      <a href="{{route('delete_coupon',$coupon->id)}}"  onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger btn-mini ">Delete</a> &nbsp;
{{--                        <a rel="{{$coupon->id}}" rel1="delete-coupon"  href="javascript:" class="btn btn-danger btn-mini deleteRecord">Delete</a>--}}

                    </td>

                  </tr>



                @endforeach
                </tbody>



              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection
