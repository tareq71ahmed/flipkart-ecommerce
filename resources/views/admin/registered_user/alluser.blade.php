@extends('layouts.adminLayout.admin_design')
@section('content')

    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a href="#">User</a> <a href="#" class="current">All User</a> </div>
            <h1>View All User</h1>
        </div>


        @if(Session::has('error'))
            <div class="alert alert-error alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! session('error') !!}</strong>
            </div>
        @endif
        @if(Session::has('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{!! session('success') !!}</strong>
            </div>
        @endif


        <div class="container-fluid">
            <hr>
            <div class="row-fluid">
                <div class="span12">

                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"><i class="icon-th"></i></span>
                            <h5>View All User</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <table class="table table-bordered data-table">
                                <thead>
                                <tr>
                                    <th>User ID</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Pincode</th>
                                    <th>mobile</th>
                                    <th>Email</th>
                                    <th>Status</th>
                                    <th>registered On</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($users as $usr)


                                    <tr class="gradeX">
                                        <td>{{$usr->id}}</td>
                                        <td>{{$usr->name}}</td>
                                        <td>
                                           {{$usr->profile->address}}
                                        </td>

                                        <td>
                                            {{$usr->profile->pincode}}
                                        </td>
                                        <td>

                                            {{$usr->profile->mobile}}

                                        </td>

                                        <td>
                                            {{$usr->email}}
                                        </td>
                                        <td>
                                            @if($usr->status===1)
                                                <h6 class="text-success font-weight-bold">1</h6>

                                            @else
                                                <h6 class="text-warning font-weight-bold">0</h6>
                                            @endif

                                        </td>

                                        <td>
                                            {{$usr->created_at}}

                                        </td>

                                        <td class="center">
                                            <a href="#exampleModal{{$usr->id}}" data-toggle="modal" data-target="#exampleModal{{$usr->id}}" class="btn btn-success btn-mini" title="View Product">View</a>
                                        </td>

                                    </tr>


                                    <div class="modal fade" id="exampleModal{{$usr->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">{{$usr->id}}</h5>
{{--                                                                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                                                                                                                <span aria-hidden="true">&times;</span>--}}
{{--                                                                                                                            </button>--}}
                                                </div>
                                                <div class="modal-body">

                                                    <p>User ID : <b>{{$usr->id}} </b></p>
                                                    <p>city : <b>{{$usr->profile->city}}</b></p>
                                                    <p>state: <b>{{$usr->profile->state}}</b></p>
                                                    <p>Country: <b> {{$usr->profile->country}}</b></p>
                                                    <p>Current_sign_in_at: <b>{{\Carbon\Carbon::parse($usr->current_sign_in_at)->diffForHumans()}}</b></p>
                                                    <p>Last_sign_in_at: <b>{{\Carbon\Carbon::parse($usr->last_sign_in_at)->diffForHumans()}}</b></p>
                                                    <p>User last url click: <b>{{$usr->user_click}}</b></p>
                                                    <p>User last click time: <b>{{\Carbon\Carbon::parse($usr->user_click_time)->diffForHumans()}}</b></p>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-danger btn-mini" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
