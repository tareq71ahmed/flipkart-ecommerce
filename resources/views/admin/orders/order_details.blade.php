

@extends('layouts.adminLayout.admin_design')
@section('content')


  <!--main-container-part-->
  <div id="content">
    <div id="content-header">
      <div id="breadcrumb"><a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a> <a
          href="#" class="current">Orders</a></div>
      <h1>Order id # {{$ordersDetails->id}}</h1>
      @if(Session::has('flash_message_success'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{!! session('flash_message_success') !!}</strong>
        </div>
      @endif
    </div>
    <div class="container-fluid">
      <hr>
      <div class="row-fluid">
        <div class="span6">
          <div class="widget-box">
            <div class="widget-title">
              <h5>Order Details</h5>
            </div>
            <div class="widget-content nopadding">
              <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                  <td class="taskDesc">Order Date</td>
                  <td class="taskStatus">{{$ordersDetails->created_at}}</td>
                </tr>
                <tr>
                  <td class="taskDesc">Order Status</td>
                  <td class="taskStatus">{{$ordersDetails->order_status}} </td>
                </tr>
                <tr>
                  <td class="taskDesc">Order Total</td>
                  <td class="taskStatus">${{$ordersDetails->grand_total}} | (BDT {{$ordersDetails->grand_total*80}})</td>
                </tr>
                <tr>
                  <td class="taskDesc">Shipping Charges</td>
                  <td class="taskStatus">${{$ordersDetails->shipping_charges}} | (BDT {{$ordersDetails->shipping_charges*80}})  </td>
                </tr>
                <tr>
                  <td class="taskDesc">Coupon Code</td>
                  <td class="taskStatus">{{$ordersDetails->coupon_code}}</td>
                </tr>
                <tr>
                  <td class="taskDesc">Coupon Amount</td>
                  <td class="taskStatus">${{$ordersDetails->coupon_amount}} | (BDT {{$ordersDetails->coupon_amount*80}}) </td>
                </tr>
                <tr>
                  <td class="taskDesc">Payment Method</td>
                  <td class="taskStatus">{{$ordersDetails->payment_method}} </td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>




          <div class="accordion" id="collapse-group">
            <div class="accordion-group widget-box">
              <div class="accordion-heading">
                <div class="widget-title">
                  <h5>Billing Address</h5>
                </div>
              </div>
              <div class="collapse in accordion-body" id="collapseGOne">
                <div class="widget-content">
                  <strong>Name:</strong> {{$billing_address->user->name}}<br>
                  <strong>Address:</strong>{{$billing_address->address}}<br>
                  <strong>City:</strong>{{$billing_address->city}}<br>
                  <strong>State:</strong>{{$billing_address->state}}<br>
                  <strong>Country:</strong>{{$billing_address->country}}<br>
                  <strong>pincode:</strong>{{$billing_address->pincode}}<br>
                  <strong>Mobile:</strong>{{$billing_address->mobile}}<br>
                </div>
              </div>
            </div>
          </div>
        </div>



        <div class="span6">
          <div class="widget-box">
            <div class="widget-title">
              <h5>Customer Details</h5>
            </div>
            <div class="widget-content nopadding">
              <table class="table table-striped table-bordered">
                <tbody>
                <tr>
                  <td class="taskDesc">Customer Name</td>
                  <td class="taskStatus">{{$ordersDetails->name}}</td>
                </tr>
                <tr>
                  <td class="taskDesc">Customer Email</td>
                  <td class="taskStatus">{{$ordersDetails->user_email}}  </td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>





          <div class="accordion" id="collapse-group">
            <div class="accordion-group widget-box">
              <div class="accordion-heading">
                <div class="widget-title">
                  <h5>Update Order Status</h5>
                </div>
              </div>

              <div class="collapse in accordion-body" id="collapseGOne">
                <div class="widget-content">

                {{--   update order status--}}
                  <form action="{{route('updateOrderStatus')}}" method="post">
                      @csrf
                    <input type="hidden" name="order_id" value="{{ $ordersDetails->id }}">
                    <table width="100%">
                      <tr>
                        <td>
                          <select name="order_status" id="order_status" class="control-label"
                              required="">
                            <option value="New" @if($ordersDetails->order_status == "New") selected @endif>New</option>
                            <option value="Pending" @if($ordersDetails->order_status == "Pending") selected @endif>Pending</option>
                            <option value="Cancelled" @if($ordersDetails->order_status == "Cancelled") selected @endif>Cancelled</option>
                            <option value="In Process" @if($ordersDetails->order_status == "In Process") selected @endif>In Process</option>
                            <option value="Shipped" @if($ordersDetails->order_status == "Shipped") selected @endif>Shipped</option>
                            <option value="Delivered" @if($ordersDetails->order_status == "Delivered") selected @endif>Delivered</option>
                            <option value="Paid" @if($ordersDetails->order_status == "Paid") selected @endif>Paid</option>
                          </select>
                        </td>
                        <td>
                          <input class="btn  btn-success" type="submit" value="Update Status">

                        </td>
                      </tr>
                    </table>
                  </form>

                    {{--   update order status End--}}

                </div>
              </div>
            </div>
          </div>


          <div class="accordion" id="collapse-group">
            <div class="accordion-group widget-box">
              <div class="accordion-heading">
                <div class="widget-title">
                  <h5>Shipping Address</h5>
                </div>
              </div>
              <div class="collapse in accordion-body" id="collapseGOne">
                <div class="widget-content">
                  <strong>Name</strong>  {{$shipping_address->name}}<br>
                  <strong>Address</strong> {{$shipping_address->address}}<br>
                  <strong>City</strong> {{$shipping_address->city}}<br>
                  <strong>State</strong> {{$shipping_address->state}}<br>
                  <strong>Country</strong>  {{$shipping_address->country}}<br>
                  <strong>Pincode </strong> {{$shipping_address->pincode}}<br>
                  <strong>Mobile</strong>  {{$shipping_address->mobile}}<br></div>
              </div>
            </div>
          </div>




        </div>
      </div>


      <div class="row-fluid">
        <h4>View Order</h4>
        <table id="example" class="table table-striped table-bordered" style="width:100%">
          <thead>
          <tr>
            <th>Product Code</th>
            <th>Product Name</th>
            <th>Product Size</th>
            <th>Product Color</th>
            <th>Product Price</th>
            <th>Product Qty</th>
          </tr>
          </thead>
          <tbody>

          @foreach($ordersDetails->order_products as $order_product)
            <tr>

              <td>{{$order_product->product_code }}</td>
              <td>{{$order_product->product_name }}</td>
              <td>{{$order_product->product_size}} </td>
              <td>{{$order_product->product_color}} </td>
              <td>${{$order_product->product_price }} | (BDT {{$order_product->product_price*80 }})</td>
              <td>{{$order_product->product_qty}} </td>

            </tr>
          @endforeach

          </tbody>
        </table>
      </div>


    </div>
  </div>
  <!--main-container-part-->



@endsection
