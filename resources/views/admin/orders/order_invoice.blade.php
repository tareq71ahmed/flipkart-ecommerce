



    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="invoice-title">
                    <h2>Invoice</h2><h3 class="pull-right">Order id # {{$ordersDetails->id}}</h3>
                    <img src="{{ asset('images/frontend_images/home/logo.png') }}">
                </div>
                <hr>
                <div class="row">
                    <div class="col-xs-6">
                        <address>
                            <strong>Billed To:</strong><br>
                            {{$billing_address->user->name}}<br>
                            {{$billing_address->address}}<br>
                            {{$billing_address->city}} <br>
                            {{$billing_address->state}}<br>
                            {{$billing_address->country}}<br>
                            {{$billing_address->pincode}}<br>
                            {{$billing_address->mobile}}<br>
                        </address>
                    </div>
                    <div class="col-xs-6 text-right">
                        <address>
                            <strong>Shipped To:</strong><br>
                            {{$shipping_address->name}}<br>
                            {{$shipping_address->address}}<br>
                            {{$shipping_address->city}}<br>
                            {{$shipping_address->state}}<br>
                            {{$shipping_address->country}}<br>
                            {{$shipping_address->pincode}}<br>
                            {{$shipping_address->mobile}}
                        </address>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <address>
                            <strong>Payment Method:</strong><br>
                            {{$ordersDetails->payment_method}}
                        </address>
                    </div>
                    <div class="col-xs-6 text-right">
                        <address>
                            <strong>Order Date:</strong><br>
                            {{$ordersDetails->created_at}} <br><br>
                        </address>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Order summary</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-condensed">
                                <thead>
                                <tr>
                                    <td style="width:12%"><strong>Product Code</strong></td>
                                    <td style="width:12%" class="text-center"><strong>Size</strong></td>
                                    <td style="width:12%" class="text-center"><strong>Color</strong></td>
                                    <td style="width:12%" class="text-center"><strong>Price</strong></td>
                                    <td style="width:12%" class="text-center"><strong>Qty</strong></td>
                                    <td style="width:12%" class="text-right"><strong>Totals</strong></td>
                                </tr>
                                </thead>
                                <tbody>



                                <?php $Subtotal = 0; ?>
                                @foreach($ordersDetails->order_products as $order_product)

                                    <tr>
                                        <td class="text-left">{{$order_product->product_code}} </td>
                                        <td class="text-center">{{$order_product->product_size}} </td>
                                        <td class="text-center">{{$order_product->product_color}} </td>
                                        <td class="text-center">{{$order_product->product_price}} </td>
                                        <td class="text-center">{{$order_product->product_qty }} </td>
                                       <td class="text-right">$ {{$order_product->product_price * $order_product->product_qty}}</td>
                                    </tr>
                                    <?php $Subtotal = $Subtotal + ($order_product->product_price *$order_product->product_qty); ?>
                                    @endforeach


                                <tr>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line"></td>
                                    <td class="thick-line text-center"><strong>Subtotal</strong></td>
                                    <td class="thick-line text-right">$ {{$Subtotal}} | (BDT {{$Subtotal*80}})</td>
                                </tr>



                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center"><strong>Shipping Charges (+)</strong></td>
                                    <td class="no-line text-right">$ {{$ordersDetails->shipping_charges}} |  (BDT {{$ordersDetails->shipping_charges*80}})</td>
                                </tr>



                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center"><strong>Coupon Discount (-)</strong></td>
                                    <td class="no-line text-right">$ {{$ordersDetails->coupon_amount}} | (BDT {{$ordersDetails->coupon_amount*80}})</td>
                                </tr>
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center"><strong>Grand Total</strong></td>
                                    <td class="no-line text-right">$ {{ $ordersDetails->grand_total}} | (BDT {{ $ordersDetails->grand_total*80}})</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





